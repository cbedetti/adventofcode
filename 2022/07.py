"""--- Day 7: No Space Left On Device ---"""

from adventofcode.puzzle import Puzzle

from collections import defaultdict


class FileSystem:
    def explore(self, cmd_lines):
        self.sizes = defaultdict(int)
        for command in cmd_lines:
            if command.startswith("$ cd"):
                self.cd(command.split()[-1])

            elif command.startswith(("$ ls", "dir")):
                pass

            else:
                size = int(command.split()[0])
                for index in range(len(self.pwd)):
                    pwd_str = "".join(self.pwd[: index + 1])
                    self.sizes[pwd_str] += size

    def cd(self, directory):
        if directory == "/":
            self.pwd = ["/"]

        elif directory == "..":
            self.pwd.pop()

        else:
            self.pwd.append(directory + "/")

    def part_one(self):
        total = 0
        for size in self.sizes.values():
            if size <= 100000:
                total += size
        return total

    def part_two(self):
        disk_size = 70000000
        update_size = 30000000
        unused_space = disk_size - self.sizes["/"]
        needed = update_size - unused_space
        deleting = update_size
        for size in self.sizes.values():
            if needed <= size < deleting:
                deleting = size
        return deleting


class Soluce(Puzzle):
    test_solutions = [
        [95437],  # Part One
        [24933642],  # Part Two
    ]

    def parser(self):
        self.filesystem = FileSystem()
        self.filesystem.explore(self.input)

    def part_one(self):
        return self.filesystem.part_one()

    def part_two(self):
        return self.filesystem.part_two()


if __name__ == "__main__":
    Soluce().solve()
