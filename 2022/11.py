"""--- Day 11: Monkey in the Middle ---"""

from adventofcode.puzzle import Puzzle

import numpy as np
from functools import cache


@cache
def is_divisible(num, by):
    return not num % by


class Soluce(Puzzle):
    test_solutions = [
        [10605],  # Part One
        [2713310158],  # Part Two
    ]

    def parse_a_monkey(self, monkey):
        num = int(monkey[0].split()[1][:-1])
        monkey_params = {
            "operation": eval(f"lambda old: {monkey[2].split('=')[-1]}"),
            "test": int(monkey[3].split()[-1]),
            True: int(monkey[4].split()[-1]),
            False: int(monkey[5].split()[-1]),
            "inspected": 0,
            "items": list(map(int, monkey[1].split(":")[-1].split(","))),
        }
        return num, monkey_params

    def parser(self):
        monkeys = [_.split("\n") for _ in "\n".join(self.input).split("\n\n")]
        self.num_monkeys = len(monkeys)
        self.monkeys = [None] * self.num_monkeys
        for monkey_raw in monkeys:
            num, monkey = self.parse_a_monkey(monkey_raw)
            self.monkeys[num] = monkey
        self.therapy = np.prod([monkey["test"] for monkey in self.monkeys])

    def do_n_round(self, num):
        for _ in range(num):
            self.do_a_round()

    def do_a_round(self):
        for monkey in self.monkeys:
            monkey["inspected"] += len(monkey["items"])
            for item in monkey["items"]:
                worry_level = monkey["operation"](item)

                if self.worried_a_bit:
                    worry_level //= 3
                else:
                    worry_level %= self.therapy

                target = monkey[is_divisible(worry_level, monkey["test"])]
                self.monkeys[target]["items"].append(worry_level)
            monkey["items"] = []

    @property
    def monkey_business(self):
        inspected = [monkey["inspected"] for monkey in self.monkeys]
        return np.prod(sorted(inspected)[-2:])

    def part_one(self):
        self.worried_a_bit = True
        self.do_n_round(20)
        return self.monkey_business

    def part_two(self):
        self.worried_a_bit = False
        self.do_n_round(10000)
        return self.monkey_business


if __name__ == "__main__":
    Soluce().solve()
