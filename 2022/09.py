"""--- Day 9: Rope Bridge ---"""

from adventofcode.puzzle import Puzzle

from math import copysign


def sign(num: float) -> float:
    if num:
        return copysign(1, num)
    else:
        return 0.0


class Rope:
    def __init__(self, starting_position, size):
        self.knots = [starting_position] * size
        self.visited = set([starting_position])

    def apply_step(self, direction):
        self.knots[0] += direction
        for index, knot in enumerate(self.knots[1:]):
            ahead_knot = self.knots[index]
            vector = ahead_knot - knot

            if abs(vector) >= 2:
                self.knots[index + 1] += complex(sign(vector.real), sign(vector.imag))

        self.visited.add(self.knots[-1])

    def apply_move(self, direction, distance):
        directions = {
            "R": 1,
            "U": 1j,
            "L": -1,
            "D": -1j,
        }
        for _ in range(int(distance)):
            self.apply_step(directions[direction])

    def apply_moves(self, instructions):
        for instruction in instructions:
            self.apply_move(*instruction.split())


class Soluce(Puzzle):
    test_solutions = [
        [13],  # Part One
        [1, 36],  # Part Two
    ]

    def part_one(self):
        rope = Rope(0j, 2)
        rope.apply_moves(self.input)
        return len(rope.visited)

    def part_two(self):
        rope = Rope(0j, 10)
        rope.apply_moves(self.input)
        return len(rope.visited)


if __name__ == "__main__":
    Soluce().solve()
