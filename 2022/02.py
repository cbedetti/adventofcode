"""--- Day 2: Rock Paper Scissors ---"""

from adventofcode.puzzle import Puzzle


# Rock: A, X (1)
# Paper: B, Y (2)
# Scissors: C, Z (3)
# L(0), D(3), W(6)
# X(L), Y(D), Z(W)

SCORING_ONE = {
    "A X": 4,
    "B X": 1,
    "C X": 7,
    "A Y": 8,
    "B Y": 5,
    "C Y": 2,
    "A Z": 3,
    "B Z": 9,
    "C Z": 6,
}

SCORING_TWO = {
    "A X": 3,
    "B X": 1,
    "C X": 2,
    "A Y": 4,
    "B Y": 5,
    "C Y": 6,
    "A Z": 8,
    "B Z": 9,
    "C Z": 7,
}


class Soluce(Puzzle):
    test_solutions = [
        [15],  # Part One
        [12],  # Part Two
    ]

    def part_one(self):
        return sum([SCORING_ONE[_] for _ in self.input])

    def part_two(self):
        return sum([SCORING_TWO[_] for _ in self.input])


if __name__ == "__main__":
    Soluce().solve()
