"""--- Day 8: Treetop Tree House ---"""

from adventofcode.puzzle import Puzzle

import numpy as np


def visible_from_a_side(line):
    visible = np.zeros_like(line, int)
    current_height = -1
    for index, height in enumerate(line):
        if height > current_height:
            visible[index] = 1
            current_height = height
    return visible


def scenic_scoring(line):
    scenic_scores = np.zeros_like(line, int)
    scenic_scores[-2] = 1
    for index_tree, height_tree in enumerate(line[1:-2]):
        for height_view in line[index_tree + 2 :]:
            scenic_scores[index_tree + 1] += 1
            if height_view >= height_tree:
                break
    return scenic_scores


class Soluce(Puzzle):
    test_solutions = [
        [21],  # Part One
        [8],  # Part Two
    ]

    def parser(self):
        self.forest = np.array([list(_) for _ in self.input], int)

    def part_one(self):
        self.visible_trees = np.zeros_like(self.forest, int)
        sides = 4
        for direction in range(sides):
            self.visible_trees += np.rot90(
                np.apply_along_axis(
                    visible_from_a_side, 0, np.rot90(self.forest, direction)
                ),
                sides - direction,
            )
        return sum(self.visible_trees.flatten() > 0)

    def part_two(self):
        self.scenic_scores = np.ones_like(self.forest, int)
        sides = 4
        for direction in range(sides):
            self.scenic_scores *= np.rot90(
                np.apply_along_axis(
                    scenic_scoring, 0, np.rot90(self.forest, direction)
                ),
                sides - direction,
            )
        return max(self.scenic_scores.flatten())

    def vizu(self):
        self.savefig(self.forest, "08_forrest.png")
        self.savefig(self.visible_trees, "08_part01.png")
        self.savefig(self.scenic_scores, "08_part02.png")


if __name__ == "__main__":
    soluce = Soluce()
    soluce.solve()
    soluce.vizu()
