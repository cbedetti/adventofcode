"""--- Day 4: Camp Cleanup ---"""

from adventofcode.puzzle import Puzzle


def contains(sectors1, sectors2):
    start1, end1 = sectors1
    start2, end2 = sectors2
    return start1 >= start2 and end1 <= end2


def overlaps(sectors1, sectors2):
    start1, end1 = sectors1
    start2, _ = sectors2
    if start1 > start2:
        return False
    else:
        return not end1 < start2


class Soluce(Puzzle):
    test_solutions = [
        [2],  # Part One
        [4],  # Part Two
    ]

    def parser(self):
        self.pairs = [
            [list(map(int, __.split("-"))) for __ in _.split(",")] for _ in self.input
        ]

    def compute(self, func):
        result = 0
        for sectors1, sectors2 in self.pairs:
            result += func(sectors1, sectors2) or func(sectors2, sectors1)
        return result

    def part_one(self):
        return self.compute(contains)

    def part_two(self):
        return self.compute(overlaps)


if __name__ == "__main__":
    Soluce().solve()
