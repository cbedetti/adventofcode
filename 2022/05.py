"""--- Day 5: Supply Stacks ---"""

from adventofcode.puzzle import Puzzle


class Soluce(Puzzle):
    test_solutions = [
        ["CMZ"],  # Part One
        ["MCD"],  # Part Two
    ]

    def parser(self):
        drawing, self.instructions = [
            _.split(".") for _ in ".".join(self.input).split("..")
        ]

        stack_n = len(drawing[-1].split())
        self.stacks = [[] for _ in range(stack_n + 1)]
        iter_stacks = enumerate(self.stacks)
        next(iter_stacks)
        for stack_number, stack in iter_stacks:
            stack_index = drawing[-1].index(str(stack_number))
            for line in drawing[-2::-1]:
                crate = line[stack_index]
                if crate == " ":
                    break
                else:
                    stack.append(crate)

    def crate_mover(self, model):
        for instruction in self.instructions:
            instruction = instruction.split()
            crate_to_move = int(instruction[1])
            origin = self.stacks[int(instruction[3])]
            target = self.stacks[int(instruction[-1])]

            if model == "9000":
                target.extend(origin[: -crate_to_move - 1 : -1])
                del origin[-crate_to_move:]

            elif model == "9001":
                target.extend(origin[-crate_to_move:])
                del origin[-crate_to_move:]

        return "".join(_[-1] for _ in self.stacks[1:])

    def part_one(self):
        return self.crate_mover("9000")

    def part_two(self):
        return self.crate_mover("9001")


if __name__ == "__main__":
    Soluce().solve()
    # https://www.reddit.com/r/adventofcode/comments/zd1hqy/2022_day_5_i_know_i_am_overthinking_it/iyzvsnp/
    # Soluce().solve("large01")
    # Soluce().solve("large02")
