"""--- Day 14: Regolith Reservoir ---"""

from adventofcode.puzzle import Puzzle


def SIGN(num):
    return (num > 0) - (num < 0)


class Soluce(Puzzle):
    test_solutions = [
        [24],  # Part One
        [93],  # Part Two
    ]

    def parser(self):
        walls = [
            [complex(*map(int, corner.split(","))) for corner in wall.split("->")]
            for wall in self.input
        ]
        self.rocks = set()
        for wall in walls:
            for corner0, corner1 in zip(wall, wall[1:]):
                vector = corner1 - corner0
                unit = complex(SIGN(vector.real), SIGN(vector.imag))
                for num in range(int(abs(vector))):
                    self.rocks.add(corner0 + num * unit)
            self.rocks.add(wall[-1])
        self.floor = max(rock.imag for rock in self.rocks)
        self.source = 500 + 0j

    def produce_sand(self):
        DOWN = 1j
        LEFT = -1 + 1j
        RIGHT = 1 + 1j
        sand = self.source
        while True:
            if sand + DOWN in self.rocks:
                if sand + LEFT in self.rocks:
                    if sand + RIGHT in self.rocks:
                        self.rocks.add(sand)
                        return sand
                    else:
                        sand += RIGHT
                else:
                    sand += LEFT
            else:
                sand += DOWN

            if sand.imag == self.floor:
                return

    def part_one(self):
        self.resting = []
        while True:
            if sand := self.produce_sand():
                self.resting.append(sand)
            else:
                break
        return len(self.resting)

    def part_two(self):
        self.floor += 2
        resting = set([self.source])
        flowing = [self.source]
        while flowing:
            sand = flowing.pop(0)
            for new_sand in [sand + adj for adj in [1j, -1 + 1j, 1 + 1j]]:
                if not (
                    new_sand in self.rocks
                    or new_sand in resting
                    or new_sand.imag == self.floor
                ):
                    resting.add(new_sand)
                    flowing.append(new_sand)
        return len(resting)


if __name__ == "__main__":
    Soluce().solve()
