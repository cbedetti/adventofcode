"""--- Day 3: Rucksack Reorganization ---"""

from adventofcode.puzzle import Puzzle

from string import ascii_letters


def grouper(iterable, size):
    for num in range(0, len(iterable), size):
        yield iterable[num : num + size]


class Soluce(Puzzle):
    test_solutions = [
        [157],  # Part One
        [70],  # Part Two
    ]

    def part_one(self):
        priorities = 0
        for rucksack in self.input:
            middle = len(rucksack) // 2
            comp1 = rucksack[:middle]
            comp2 = rucksack[middle:]
            for item in comp1:
                if item in comp2:
                    priorities += ascii_letters.index(item) + 1
                    break
        return priorities

    def part_two(self):
        priorities = 0
        for groups in grouper(self.input, 3):
            for item in groups[0]:
                if item in groups[1] and item in groups[2]:
                    priorities += ascii_letters.index(item) + 1
                    break
        return priorities


if __name__ == "__main__":
    Soluce().solve()
