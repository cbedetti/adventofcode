"""--- Day 6: Tuning Trouble ---"""

from adventofcode.puzzle import Puzzle


class Soluce(Puzzle):
    test_solutions = [
        [5, 6, 10, 11],  # Part One
        [23, 23, 29, 26, 19],  # Part Two
    ]

    def reparing_the_device(self, window_size):
        for start in range(len(self.input) - window_size):
            end = start + window_size
            if len(set(self.input[start:end])) == window_size:
                return end

    def part_one(self):
        return self.reparing_the_device(4)

    def part_two(self):
        return self.reparing_the_device(14)


if __name__ == "__main__":
    Soluce().solve()
