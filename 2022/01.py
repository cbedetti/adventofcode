"""--- Day 1: Calorie Counting ---"""

from adventofcode.puzzle import Puzzle


class Soluce(Puzzle):
    test_solutions = [
        [24000],  # Part One
        [45000],  # Part Two
    ]

    def parser(self):
        elves = ",".join(self.input).split(",,")
        self.calories = [sum(map(int, _.split(","))) for _ in elves]

    def part_one(self):
        return max(self.calories)

    def part_two(self):
        return sum(sorted(self.calories)[-3:])


if __name__ == "__main__":
    Soluce().solve()
