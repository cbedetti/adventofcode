#!/bin/bash

rm -Rf venv/adventofcode
python3 -m venv venv/adventofcode
source venv/adventofcode/bin/activate
pip install -U pip
pip install -e .
echo "source venv/adventofcode/bin/activate"
