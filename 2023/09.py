"""--- Day 9: Mirage Maintenance ---"""

from adventofcode.puzzle import Puzzle

import numpy as np


def predict(values):
    next_value = values[-1]
    while np.any(values):
        values = np.diff(values)
        next_value += values[-1]
    return next_value


def backward_predict(values):
    items = [values[0]]
    while np.any(values):
        values = np.diff(values)
        items.append(values[0])

    previous_value = 0
    for item in reversed(items):
        previous_value = item - previous_value
    return previous_value


class Soluce(Puzzle):
    test_solutions = [
        [114],  # Part One
        [2],  # Part Two
    ]

    def parser(self):
        self.oasis = [[*map(int, _.split())] for _ in self.input]

    def part_one(self):
        return sum(map(predict, self.oasis))

    def part_two(self):
        return sum(map(backward_predict, self.oasis))


if __name__ == "__main__":
    Soluce().solve()
