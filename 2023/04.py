"""--- Day 4: Scratchcards ---"""

from adventofcode.puzzle import Puzzle

from collections import defaultdict


class Soluce(Puzzle):
    test_solutions = [
        [13],  # Part One
        [30],  # Part Two
    ]

    def parser(self):
        self.cards = []
        for card_raw in self.input:
            card_id, numbers = card_raw.split(":")
            card_id = card_id.split()[-1]
            winning, number = [set(_.split()) for _ in numbers.split("|")]
            self.cards.append((int(card_id), len(winning.intersection(number))))

    def part_one(self):
        points = 0
        for _, winning_numbers in self.cards:
            if winning_numbers:
                points += 2 ** (winning_numbers - 1)
        return points

    def part_two(self):
        scratchcards = 0
        copies = defaultdict(lambda: 1)
        for card_id, winning_number in self.cards:
            scratchcards += copies[card_id]
            for shift in range(1, winning_number + 1):
                copies[card_id + shift] += copies[card_id]
        return scratchcards


if __name__ == "__main__":
    Soluce().solve()
