"""--- Day 1: Trebuchet?! ---"""

from adventofcode.puzzle import Puzzle

import re


LETTERS_TO_DIGIT = {
    "one": "1",
    "two": "2",
    "three": "3",
    "four": "4",
    "five": "5",
    "six": "6",
    "seven": "7",
    "eight": "8",
    "nine": "9",
}


def calibrate(calibration_value):
    digits = re.findall(r"\d", calibration_value)
    return int(digits[0] + digits[-1])


def translate_slow(calibration_value):
    translation = ""
    scanning = ""
    for letter in calibration_value:
        scanning += letter

        if letter.isdigit():
            translation += letter
            scanning = ""

        elif len(scanning) < 3:
            continue

        else:
            for letters, digit in LETTERS_TO_DIGIT.items():
                if letters in scanning:
                    translation += digit
                    scanning = scanning[-len(letters) + 1 :]
                    break

    return int(translation[0] + translation[-1])


def translate(calibration_value):
    pattern = "(?=(\d|{}))".format("|".join(LETTERS_TO_DIGIT.keys()))
    digits = re.findall(pattern, calibration_value)
    first, last = digits[0], digits[-1]
    return int(LETTERS_TO_DIGIT.get(first, first) + LETTERS_TO_DIGIT.get(last, last))


class Soluce(Puzzle):
    test_solutions = [
        [142],  # Part One
        [None, 281],  # Part Two
    ]

    def part_one(self):
        return sum(map(calibrate, self.input))

    def part_two(self):
        return sum(map(translate, self.input))


if __name__ == "__main__":
    Soluce().solve()
