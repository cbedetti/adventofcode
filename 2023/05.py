"""--- Day 5: If You Give A Seed A Fertilizer ---"""

from adventofcode.puzzle import Puzzle

import re


class Soluce(Puzzle):
    test_solutions = [
        [35],  # Part One
        [46],  # Part Two
    ]

    def parser(self):
        data = [_.split("\n") for _ in "\n".join(self.input).split("\n\n")]
        self.seeds = list(map(int, re.findall(r"\d+", data[0][0])))
        self.steps = []
        for step in data[1:]:
            maps = []
            for mapping in step[1:]:
                destination, start, length = map(int, re.findall(r"\d+", mapping))
                maps.append((start, start + length, destination - start))
            self.steps.append(maps)

    def part_one(self):
        def apply_step(seed, step):
            for start, end, shift in step:
                if start <= seed < end:
                    return seed + shift
            return seed

        for step in self.steps:
            self.seeds = [apply_step(seed, step) for seed in self.seeds]

        return min(self.seeds)

    def part_two(self):
        seeds = []
        while self.seeds:
            length = self.seeds.pop()
            start = self.seeds.pop()
            seeds.append([start, start + length])

        for step in self.steps:
            seeds_next_step = []
            for start_mapping, end_mapping, shift in step:
                seeds_next_mapping = []
                for seed_interval in seeds:
                    start_seed, end_seed = seed_interval
                    start = max(start_seed, start_mapping)
                    end = min(end_seed, end_mapping)

                    if start >= end:
                        # No intersection
                        seeds_next_mapping.append(seed_interval)

                    else:
                        seeds_next_step.append([start + shift, end + shift])

                        if start_seed < start:
                            seeds_next_mapping.append([start_seed, start])

                        if end < end_seed:
                            seeds_next_mapping.append([end, end_seed])
                seeds = seeds_next_mapping
            seeds = seeds_next_step + seeds_next_mapping
        return min(_[0] for _ in seeds)


if __name__ == "__main__":
    Soluce().solve()
