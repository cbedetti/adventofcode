"""--- Day 2: Cube Conundrum ---"""

from adventofcode.puzzle import Puzzle

import numpy as np


COLORS = {
    "red": 0,
    "green": 1,
    "blue": 2,
}


class Soluce(Puzzle):
    test_solutions = [
        [8],  # Part One
        [2286],  # Part Two
    ]

    def parser(self):
        self.games = {}
        for input_str in self.input:
            game, all_grabs = input_str.split(":")
            game_id = int(game.split()[-1])
            grabs = []
            for cubes in all_grabs.split(";"):
                grab = [0, 0, 0]
                for cube in cubes.split(","):
                    num, color = cube.split()
                    grab[COLORS[color]] = int(num)
                grabs.append(grab)
            self.games[game_id] = np.array(grabs)

    def part_one(self):
        bag = np.array([12, 13, 14])
        sum_ids = 0
        for game_id, grabs in self.games.items():
            if np.all(grabs <= bag):
                sum_ids += game_id
        return sum_ids

    def part_two(self):
        sum_of_power = 0
        for grabs in self.games.values():
            sum_of_power += np.max(grabs, 0).prod()
        return sum_of_power


if __name__ == "__main__":
    Soluce().solve()
