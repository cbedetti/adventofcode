"""--- Day 8: Haunted Wasteland ---"""

from adventofcode.puzzle import Puzzle

import re
import numpy as np
from itertools import cycle


class Soluce(Puzzle):
    test_solutions = [
        [2, 6],  # Part One
        [None, None, 6],  # Part Two
    ]

    def parser(self):
        traductions = {"L": 0, "R": 1}
        self.directions = [traductions[_] for _ in self.input[0].strip()]

        self.map = {}
        for node in self.input[2:]:
            key, values = node.split("=")
            self.map[key.strip()] = re.findall(r"\w+", values)

    def part_one(self):
        node = "AAA"
        for step, direction in enumerate(cycle(self.directions)):
            node = self.map[node][direction]
            if node == "ZZZ":
                break
        return step + 1

    def part_two(self):
        nodes = [_ for _ in self.map.keys() if _[-1] == "A"]
        node_cycles = []
        for step, direction in enumerate(cycle(self.directions)):
            next_nodes = []
            for node in nodes:
                next_node = self.map[node][direction]
                if next_node[-1] == "Z":
                    node_cycles.append(step + 1)
                else:
                    next_nodes.append(next_node)

            if next_nodes:
                nodes = next_nodes
            else:
                break

        return np.lcm.reduce(node_cycles)


if __name__ == "__main__":
    Soluce().solve()
