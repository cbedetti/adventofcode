"""--- Day 3: Gear Ratios ---"""

from adventofcode.puzzle import Puzzle

import re
from collections import defaultdict
from itertools import product


class Soluce(Puzzle):
    test_solutions = [
        [4361],  # Part One
        [467835],  # Part Two
    ]

    def parser(self):
        self.parts = set()
        self.gears = set()
        for x, line in enumerate(self.input):
            for y, element in enumerate(line):
                if not (element.isdigit() or element == "."):
                    coord = (x, y)
                    self.parts.add(coord)
                    if element == "*":
                        self.gears.add(coord)

    def part_one(self):
        sum_of_parts = 0
        for x, line in enumerate(self.input):
            for match in re.finditer(r"\d+", line):
                start, end = match.span()
                kernel = set([(x, start - 1), (x, end)])
                kernel.update(
                    (x + x_shift, y)
                    for x_shift, y in product([-1, 1], range(start - 1, end + 1))
                )

                if kernel.intersection(self.parts):
                    sum_of_parts += int(line[start:end])

        return sum_of_parts

    def part_two(self):
        gears = defaultdict(list)
        for x, line in enumerate(self.input):
            for match in re.finditer(r"\d+", line):
                start, end = match.span()
                kernel = set([(x, start - 1), (x, end)])
                kernel.update(
                    (x + x_shift, y)
                    for x_shift, y in product([-1, 1], range(start - 1, end + 1))
                )

                for gear_coord in kernel.intersection(self.gears):
                    gears[gear_coord].append(int(line[start:end]))

        sum_of_ratio = 0
        for ratio in gears.values():
            if len(ratio) == 2:
                sum_of_ratio += ratio[0] * ratio[1]

        return sum_of_ratio


if __name__ == "__main__":
    Soluce().solve()
