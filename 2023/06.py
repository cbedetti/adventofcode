"""--- Day 6: Wait For It ---"""

from adventofcode.puzzle import Puzzle

import re
import numpy as np


class Soluce(Puzzle):
    test_solutions = [
        [288],  # Part One
        [71503],  # Part Two
    ]

    def parser(self):
        self.races = []
        for line in self.input:
            self.races.append(list(map(int, re.findall(r"\d+", line))))

    def part_one(self):
        races = np.array([[-1] * len(self.races[0])] + self.races).T
        races[:, 2] = -races[:, 2]

        roots = np.sort(np.apply_along_axis(np.roots, 1, races), 1)
        roots[:, 0] = np.floor(roots[:, 0]) + 1
        roots[:, 1] = np.ceil(roots[:, 1])

        return int(np.diff(roots).prod())

    def part_two(self):
        self.races = [[int("".join(map(str, _)))] for _ in self.races]
        return self.part_one()


if __name__ == "__main__":
    Soluce().solve()
