"""--- Day 13: Point of Incidence ---"""

from adventofcode.puzzle import Puzzle

import numpy as np


def find_reflection(pattern):
    size = len(pattern)
    for row in range(1, size):
        inf, sup = 0, size
        if 2 * row < size:
            sup = 2 * row
        else:
            inf = 2 * row - size

        sub_pattern = pattern[inf:sup, :]

        if np.array_equal(sub_pattern, np.flip(sub_pattern, 0)):
            return row
    return 0


def find_smudge(pattern):
    size = len(pattern)
    for row in range(1, size):
        inf, sup = 0, size
        if 2 * row < size:
            sup = 2 * row
        else:
            inf = 2 * row - size

        sub_pattern = pattern[inf:sup, :]

        if np.sum(np.invert(sub_pattern == np.flip(sub_pattern, 0))) == 2:
            return row
    return 0


class Soluce(Puzzle):
    test_solutions = [
        [405],  # Part One
        [400],  # Part Two
    ]

    def parser(self):
        self.patterns = []
        for pattern in "\n".join(self.input).split("\n\n"):
            self.patterns.append(np.array([[*_] for _ in pattern.split("\n")]) == "#")

    def part_one(self):
        score = 100 * sum(map(find_reflection, self.patterns))
        score += sum(map(find_reflection, [np.rot90(_, -1) for _ in self.patterns]))
        return score

    def part_two(self):
        score = 100 * sum(map(find_smudge, self.patterns))
        score += sum(map(find_smudge, [np.rot90(_, -1) for _ in self.patterns]))
        return score


if __name__ == "__main__":
    Soluce().solve()
