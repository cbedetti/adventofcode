"""--- Day 20: Particle Swarm ---"""

from adventofcode.puzzle import Puzzle

import re
import numpy as np
from collections import Counter


class Soluce(Puzzle):
    test_solutions = [
        [0],  # Part One
        [None, 1],  # Part Two
    ]
    DURATION = 375

    def parser(self):
        data = np.array(
            [list(map(int, re.sub("[p=<>va]", "", _).split(","))) for _ in self.input]
        )
        self.pos = data[:, :3]
        self.vel = data[:, 3:6]
        self.acc = data[:, 6:]

    def part_one(self):
        for _ in range(self.DURATION):
            self.vel += self.acc
            self.pos += self.vel
        distances = abs(self.pos).sum(axis=1)
        return np.argmin(distances)

    def part_two(self):
        for _ in range(self.DURATION):
            collisions = Counter([tuple(pos) for pos in self.pos])
            for collision, count in collisions.most_common():
                if count == 1:
                    break
                else:
                    collisions_ind = []
                    for ind, pos in enumerate(self.pos):
                        if np.array_equal(collision, pos):
                            collisions_ind.append(ind)
                    self.pos = np.delete(self.pos, collisions_ind, axis=0)
                    self.vel = np.delete(self.vel, collisions_ind, axis=0)
                    self.acc = np.delete(self.acc, collisions_ind, axis=0)
            self.vel += self.acc
            self.pos += self.vel
        return self.pos.shape[0]


if __name__ == "__main__":
    Soluce().solve()
