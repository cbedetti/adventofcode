"""--- Day 8: I Heard You Like Registers ---"""

from adventofcode.puzzle import Puzzle

from collections import defaultdict


class Soluce(Puzzle):
    test_solutions = [
        [1],  # Part One
        [10],  # Part Two
    ]

    def parser(self):
        self.instructions = []
        for data in self.input:
            operation, condition = data.split("if ")
            if "inc" in operation:
                register, value = operation.split(" inc")
                value = int(value)
            else:
                register, value = operation.split(" dec")
                value = -int(value)
            self.instructions.append((register, value, condition))

    def part_one(self):
        memory = defaultdict(lambda: 0)
        for register, value, condition in self.instructions:
            conditions = condition.split()
            if eval(f"memory['{conditions[0]}']{' '.join(conditions[1:])}"):
                memory[register] += value
        return max(memory.values())

    def part_two(self):
        memory = defaultdict(lambda: 0)
        peak = 0
        for register, value, condition in self.instructions:
            conditions = condition.split()
            if eval(f"memory['{conditions[0]}']{' '.join(conditions[1:])}"):
                memory[register] += value
                if memory[register] > peak:
                    peak = memory[register]
        return peak


if __name__ == "__main__":
    Soluce().solve()
