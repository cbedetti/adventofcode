"""--- Day 13: Packet Scanners ---"""

from adventofcode.puzzle import Puzzle

from itertools import count


class Soluce(Puzzle):
    test_solutions = [
        [24],  # Part One
        [10],  # Part Two
    ]

    def parser(self):
        self.firewall = []
        for scanner in self.input:
            depth, size = scanner.split(": ")
            depth = int(depth)
            size = int(size)
            self.firewall.append((depth, size, 2 * (size - 1), depth * size))

    def severity(self, delay=0):
        trip_severity = 0
        caught = False
        for depth, size, frequency, severity in self.firewall:
            if (depth + delay) % frequency == 0:
                trip_severity += severity
                caught = True
        return caught, trip_severity

    def part_one(self):
        return self.severity()[1]

    def part_two(self):
        for delay in count():
            caught, trip_severity = self.severity(delay)
            if not caught and trip_severity == 0:
                break
        return delay


if __name__ == "__main__":
    Soluce().solve()
