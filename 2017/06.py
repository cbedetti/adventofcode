"""--- Day 6: Memory Reallocation ---"""

from adventofcode.puzzle import Puzzle


class Soluce(Puzzle):
    test_solutions = [
        [5],  # Part One
        [4],  # Part Two
    ]

    def parser(self):
        self.memory = [int(_) for _ in self.input.split()]

    def redistribution(self):
        step = 0
        history = {}
        history[tuple(self.memory)] = step
        memory_len = len(self.memory)
        while True:
            max_bank = self.memory.index(max(self.memory))
            boxes = self.memory[max_bank]
            self.memory[max_bank] = 0

            position = (max_bank + 1) % memory_len
            new_boxes = boxes // memory_len
            bonus = boxes % memory_len
            for _ in range(memory_len):
                if bonus > 0:
                    self.memory[position] += new_boxes + 1
                else:
                    self.memory[position] += new_boxes
                bonus -= 1
                position = (position + 1) % memory_len

            step += 1
            if tuple(self.memory) in history.keys():
                break
            else:
                history[tuple(self.memory)] = step

        return step, history[tuple(self.memory)]

    def part_one(self):
        return self.redistribution()[0]

    def part_two(self):
        step, beginning = self.redistribution()
        return step - beginning


if __name__ == "__main__":
    Soluce().solve()
