"""--- Day 9: Stream Processing ---"""

from adventofcode.puzzle import Puzzle


class Soluce(Puzzle):
    test_solutions = [
        [1, 6, 5, 16, 1, 9, 9, 3],  # Part One
        [None] * 8 + [0, 17, 3, 2, 0, 0, 10],  # Part Two
    ]

    def parser(self):
        self.score = 0
        self.within_garbage = 0

        height = 0
        escaping = False
        in_garbage = False
        for char in self.input:
            if escaping:
                escaping = False

            elif char == "!":
                escaping = True

            elif char == ">":
                in_garbage = False

            elif in_garbage:
                self.within_garbage += 1

            elif char == "<":
                in_garbage = True

            elif char == "{":
                height += 1
                self.score += height

            elif char == "}":
                height -= 1

    def part_one(self):
        return self.score

    def part_two(self):
        return self.within_garbage


if __name__ == "__main__":
    Soluce().solve()
