"""--- Day 11: Hex Ed ---"""

from adventofcode.puzzle import Puzzle


class Soluce(Puzzle):
    """
    https://stackoverflow.com/questions/5084801/manhattan-distance-between-tiles-in-a-hexagonal-grid
    with n as y-axis and ne as x-axis
    """

    test_solutions = [
        [3, 0, 2, 3],  # Part One
        [],  # Part Two
    ]
    moves = {
        "n": 1j,
        "ne": 1,
        "se": 1 - 1j,
        "s": -1j,
        "sw": -1,
        "nw": -1 + 1j,
    }

    def parser(self):
        self.directions = self.input.split(",")

    def distance_from_center(self, position):
        sign = lambda n: (n > 0) - (n < 0)
        if sign(position.real) == sign(position.imag):
            return abs(position.real + position.imag)
        else:
            return max(abs(position.real), abs(position.imag))

    def part_one(self):
        position = 0j
        for direction in self.directions:
            position += self.moves[direction]
        return int(self.distance_from_center(position))

    def part_two(self):
        position = 0j
        furthest = 0
        for direction in self.directions:
            position += self.moves[direction]
            distance = self.distance_from_center(position)
            if distance > furthest:
                furthest = distance
        return int(furthest)


if __name__ == "__main__":
    Soluce().solve()
