"""--- Day 15: Dueling Generators ---"""

from adventofcode.puzzle import Puzzle

import numpy as np


class Generator:
    def __init__(self, value, factor, chunk_size):
        self.base = 2147483647
        self.values = np.array([(value * factor) % self.base], dtype=np.int64)
        self.factor = factor
        self._doubling_factor = factor
        self.chunk_size = chunk_size
        self.chunk_factor = pow(factor, chunk_size, self.base)
        self._init_values()

    def _init_values(self):
        while len(self.values) < self.chunk_size:
            self._double_values()
        self.values = self.values[: self.chunk_size]

    def _double_values(self):
        new_values = self.values.copy()
        new_values *= self._doubling_factor
        new_values %= self.base
        self.values = np.concatenate((self.values, new_values))
        self._doubling_factor = pow(self._doubling_factor, 2, self.base)

    def compute_next_values(self):
        self.values *= self.chunk_factor
        self.values %= self.base

    def compute_values_with_criterion(self, criterion, size):
        values = np.array([], dtype=np.int64)
        while len(values) < size:
            values = np.concatenate((values, self.values[self.values % criterion == 0]))
            self.compute_next_values()
        self.values = values[:size]


def judge(generator_A, generator_B):
    mask = (1 << 16) - 1
    return generator_A.values & mask == generator_B.values & mask


class Soluce(Puzzle):
    test_solutions = [
        [588],  # Part One
        [309],  # Part Two
    ]

    FACTORS = [16807, 48271]
    PAIRS = 40e6
    CHUNK_NUMBER = 500
    assert PAIRS % CHUNK_NUMBER == 0
    CHUNK_SIZES = [int(PAIRS / CHUNK_NUMBER)] * 2
    CRITERIA = [4, 8]

    def parser(self):
        self.values = [int(_.split()[-1]) for _ in self.input]

    def part_one(self):
        generators = list(map(Generator, self.values, self.FACTORS, self.CHUNK_SIZES))
        matching = 0
        for _ in range(self.CHUNK_NUMBER):
            matching += np.sum(judge(*generators))
            for generator in generators:
                generator.compute_next_values()
        return matching

    def part_two(self):
        generators = list(map(Generator, self.values, self.FACTORS, self.CHUNK_SIZES))
        for generator, criterion in zip(generators, self.CRITERIA):
            generator.compute_values_with_criterion(criterion, int(5e6))
        return np.sum(judge(*generators))


if __name__ == "__main__":
    Soluce().solve()
