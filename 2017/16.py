"""--- Day 16: Permutation Promenade ---"""

from adventofcode.puzzle import Puzzle

from string import ascii_lowercase


class Song:
    def __init__(self, num_dancer):
        self.dancers = bytearray(ascii_lowercase[:num_dancer], "utf-8")
        self.dance_num = 0
        self.memory = {self.dance_num: self.dancers.copy()}

    def dance(self, moves):
        for command, args in moves:
            getattr(self, command)(*args)
        self.dance_num += 1
        self.memory[self.dance_num] = self.dancers.copy()

    def s(self, num):
        self.dancers = self.dancers[-num:] + self.dancers[:-num]

    def x(self, posA, posB):
        self.dancers[posA], self.dancers[posB] = self.dancers[posB], self.dancers[posA]

    def p(self, dancerA, dancerB):
        posA = self.dancers.index(dancerA)
        posB = self.dancers.index(dancerB)
        self.x(posA, posB)


class Soluce(Puzzle):
    test_solutions = [
        ["baedc"],  # Part One
        [],  # Part Two
    ]

    def parser(self):
        self.moves = []
        for move_raw in self.input.split(","):
            command, args = move_raw[0], move_raw[1:]
            if command == "s":
                args = [int(args)]
            elif command == "x":
                args = list(map(int, args.split("/")))
            else:
                args = list(map(bytearray, args.split("/"), ["utf-8"] * 2))
            self.moves.append([command, args])

    def part_one(self):
        if self.testing:
            song = Song(5)
        else:
            song = Song(16)
        song.dance(self.moves)
        return song.dancers.decode()

    def part_two(self):
        song = Song(16)
        while True:
            song.dance(self.moves)
            if song.dancers == song.memory[0]:
                break

        dance_cycle = 1_000_000_000 % (song.dance_num)
        return song.memory[dance_cycle].decode()


if __name__ == "__main__":
    Soluce().solve()
