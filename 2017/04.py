"""--- Day 4: High-Entropy Passphrases ---"""

from adventofcode.puzzle import Puzzle

from collections import Counter
from itertools import combinations


class Soluce(Puzzle):
    test_solutions = [
        [2],  # Part One
        [None, 3],  # Part Two
    ]

    def parser(self):
        self.passphrases = [_.split() for _ in self.input]

    def count(self, is_valid):
        valid = 0
        for passphrase in self.passphrases:
            valid += is_valid(passphrase)
        return valid

    def part_one(self):
        def is_valid(passphrase):
            words = set(passphrase)
            return len(passphrase) == len(words)

        return self.count(is_valid)

    def part_two(self):
        def is_valid(passphrase):
            for word1, word2 in combinations(passphrase, 2):
                if Counter(word1) == Counter(word2):
                    return False
            return True

        return self.count(is_valid)


if __name__ == "__main__":
    Soluce().solve()
