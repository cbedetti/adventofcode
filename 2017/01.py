"""--- Day 1: Inverse Captcha ---"""

from adventofcode.puzzle import Puzzle


def compute_captcha(digits, shift=1):
    captcha = 0
    len_input = len(digits)
    for n, num in enumerate(digits):
        if num == digits[(n + shift) % len_input]:
            captcha += int(num)
    return captcha


class Soluce(Puzzle):

    def part_one(self):
        return compute_captcha(self.input)

    def part_two(self):
        shift = len(self.input) // 2
        return compute_captcha(self.input, shift)


if __name__ == "__main__":
    Soluce().solve()
