"""--- Day 14: Disk Defragmentation ---"""

from adventofcode.puzzle import Puzzle
from adventofcode.utils import KnotHash

import numpy as np
from scipy.ndimage import label


class Soluce(Puzzle):
    test_solutions = [
        [8108],  # Part One
        [1242],  # Part Two
    ]

    def parser(self):
        def hex2bin(hex_str):
            return "{:04b}".format(int(hex_str, 16))

        self.grid = []
        for row_number in range(128):
            hasher = KnotHash()
            hasher.hashing(f"{self.input}-{row_number}")
            row = "".join(map(hex2bin, hasher.knot_hash))
            self.grid.append(row)
        self.grid = np.array([list(map(int, row)) for row in self.grid])

    def part_one(self):
        return self.grid.sum()

    def part_two(self):
        self.labels, num_regions = label(self.grid)
        return num_regions

    def vizu(self):
        self.save_array(self.grid, "14_part1.png")
        self.save_array(self.labels, "14_part2.png")


if __name__ == "__main__":
    soluce = Soluce()
    soluce.solve()
    soluce.vizu()
