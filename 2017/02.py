"""--- Day 2: Corruption Checksum ---"""

from adventofcode.puzzle import Puzzle

from itertools import permutations


class Soluce(Puzzle):
    test_solutions = [
        [18],  # Part One
        [],  # Part Two
    ]

    def parser(self):
        self.spreadsheet = [list(map(int, _.split())) for _ in self.input]

    def part_one(self):
        checksum = 0
        for row in self.spreadsheet:
            checksum += max(row) - min(row)
        return checksum

    def part_two(self):
        checksum = 0
        for row in self.spreadsheet:
            for item1, item2 in permutations(row, 2):
                if item1 % item2 == 0:
                    checksum += item1 // item2
                    break
        return checksum


if __name__ == "__main__":
    Soluce().solve()
