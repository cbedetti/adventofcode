"""--- Day 22: Sporifica Virus ---"""

from adventofcode.puzzle import Puzzle

from collections import defaultdict


class Soluce(Puzzle):
    test_solutions = [
        [5587],  # Part One
        [2511944],  # Part Two
    ]

    def parser(self):
        self.grid = defaultdict(lambda: 0)
        for imag, row in enumerate(self.input):
            for real, data in enumerate(row):
                if data == "#":
                    self.grid[complex(real, -imag)] = 2

        self.direction = 1j
        self.position = complex(
            len(self.input[0]) // 2,
            -(len(self.input[1]) // 2),
        )

    def part_one(self):
        infection = 0
        for _ in range(10000):
            if self.grid[self.position]:
                # turn right and clean
                self.direction *= -1j
                self.grid[self.position] = 0
            else:
                # turn left and infect
                self.direction *= 1j
                self.grid[self.position] = 2
                infection += 1
            self.position += self.direction
        return infection

    def part_two(self):
        """
        0: clean
        1: weakened
        2: infected
        3: flagged
        """
        evolution = {0: 1, 1: 2, 2: 3, 3: 0}
        infection = 0
        for _ in range(10000000):
            if self.grid[self.position] == 0:
                # turn left
                self.direction *= 1j

            elif self.grid[self.position] == 1:
                infection += 1

            elif self.grid[self.position] == 2:
                # turn right
                self.direction *= -1j

            elif self.grid[self.position] == 3:
                # reverse
                self.direction = complex(-self.direction.real, -self.direction.imag)

            self.grid[self.position] = evolution[self.grid[self.position]]
            self.position += self.direction
        return infection


if __name__ == "__main__":
    Soluce().solve()
