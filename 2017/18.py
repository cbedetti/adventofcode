"""--- Day 18: Duet ---"""

from adventofcode.puzzle import Puzzle
from adventofcode.utils import Cpu

from threading import Thread
from queue import Queue, Empty


class Speaker(Cpu):
    def __init__(self):
        super().__init__()
        self.played = []

    def snd(self, register):
        self.played.append(self.registers[register])
        self.pointer += 1

    def rcv(self, register):
        if self.registers[register] != 0:
            self.running = False
        else:
            self.pointer += 1


class TalkyWalky(Cpu):
    def __init__(self, value):
        super().__init__()
        self.inbox = Queue()
        self.registers["p"] = value
        self.friend = None
        self.send = 0

    def snd(self, register):
        if isinstance(register, str):
            value = self.registers[register]
        else:
            value = int(register)
        self.friend.inbox.put(value)
        self.send += 1
        self.pointer += 1

    def rcv(self, register):
        try:
            self.registers[register] = self.inbox.get(timeout=1e-3)
            self.pointer += 1
        except Empty:
            self.running = False


class Soluce(Puzzle):
    test_solutions = [
        [4],  # Part One
        [None, 3],  # Part Two
    ]

    def parser(self):
        self.instructions = []
        for instruction_raw in self.input:
            instruction = []
            for part in instruction_raw.split():
                try:
                    instruction.append(int(part))
                except ValueError:
                    instruction.append(part)
            self.instructions.append(instruction)

    def part_one(self):
        speaker = Speaker()
        speaker.run(self.instructions)
        return speaker.played[-1]

    def part_two(self):
        talkywalkies = [TalkyWalky(0), TalkyWalky(1)]
        talkywalkies[0].friend = talkywalkies[1]
        talkywalkies[1].friend = talkywalkies[0]

        threads = [
            Thread(target=talkywalkies[0].run, args=[self.instructions]),
            Thread(target=talkywalkies[1].run, args=[self.instructions]),
        ]
        [_.start() for _ in threads]
        [_.join() for _ in threads]

        return talkywalkies[1].send


if __name__ == "__main__":
    Soluce().solve()
