"""--- Day 24: Electromagnetic Moat ---"""

from adventofcode.puzzle import Puzzle


def build_bridge(bridge, end_pin, components):
    next_steps = []
    for component in components:
        for end, pin in component.items():
            if pin == end_pin:
                remaining = components.copy()
                remaining.remove(component)
                next_steps.append(
                    [
                        bridge + list(component.values()),
                        component[not end],
                        remaining,
                    ]
                )
                break

    if next_steps:
        for next_step in next_steps:
            yield from build_bridge(*next_step)
    else:
        yield bridge


class Soluce(Puzzle):
    test_solutions = [
        [31],  # Part One
        [19],  # Part Two
    ]

    def parser(self):
        self.components = []
        for ports in self.input:
            component = {}
            for pos, pin in zip([True, False], map(int, ports.split("/"))):
                component[pos] = pin
            self.components.append(component)

    def part_one(self):
        return max(sum(_) for _ in build_bridge([], 0, self.components))

    def part_two(self):
        bridges = [_ for _ in build_bridge([], 0, self.components)]
        strenghts = map(sum, bridges)
        sizes = list(map(len, bridges))
        longest = max(sizes)
        strongest = 0
        for size, strenght in zip(sizes, strenghts):
            if size == longest and strenght > strongest:
                strongest = strenght
        return strongest


if __name__ == "__main__":
    Soluce().solve()
