"""--- Day 19: A Series of Tubes ---"""

from adventofcode.puzzle import Puzzle

from collections import defaultdict


class Soluce(Puzzle):
    test_solutions = [
        ["ABCDEF"],  # Part One
        [38],  # Part Two
    ]

    def parser(self):
        self.position = complex(self.input[0].index("|"))
        self.direction = 1j
        self.grid = defaultdict(lambda: " ")
        for imag, row in enumerate(self.input):
            for real, data in enumerate(row):
                self.grid[complex(real, imag)] = data

    def search_new_direction(self):
        exclude = {
            1: "-",
            -1: "-",
            1j: "|",
            -1j: "|",
        }
        for side in [1j, -1j]:
            new_direction = self.direction * side
            looking = self.position + new_direction
            if not self.grid[looking] in [" ", exclude[self.direction]]:
                self.direction = new_direction

    def packet(self):
        message = ""
        steps = 0
        while True:
            self.position += self.direction
            steps += 1
            data = self.grid[self.position]
            if data == " ":
                break
            elif data in ["|", "-"]:
                continue
            elif data == "+":
                self.search_new_direction()
            else:
                message += data
        return message, steps

    def part_one(self):
        return self.packet()[0]

    def part_two(self):
        return self.packet()[1]


if __name__ == "__main__":
    Soluce().solve()
