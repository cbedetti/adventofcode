"""--- Day 10: Knot Hash ---"""

from adventofcode.puzzle import Puzzle
from adventofcode.utils import KnotHash


class Soluce(Puzzle):
    test_solutions = [
        [12],  # Part One
        [
            None,
            "a2582a3a0e66e6e86e3812dcb672a272",
            "33efeb34ea91902bb2f59c9920caa6cd",
            "3efbe78a8d82f29979031a4aa0b16a9d",
            "63960835bcdc130f0b66d7ff4f6a5a8e",
        ],  # Part Two
    ]

    def part_one(self):
        if self.testing:
            hasher = KnotHash(mem_size=5)
        else:
            hasher = KnotHash()
        sequence = [int(_) for _ in self.input.split(",")]
        hasher.hashing(sequence, repeat=1)
        return hasher.memory[0] * hasher.memory[1]

    def part_two(self):
        hasher = KnotHash()
        hasher.hashing(self.input)
        return hasher.knot_hash


if __name__ == "__main__":
    Soluce().solve()
