"""--- Day 7: Recursive Circus ---"""

from adventofcode.puzzle import Puzzle

from collections import Counter, defaultdict


class Soluce(Puzzle):
    test_solutions = [
        ["tknk"],  # Part One
        [60],  # Part Two
    ]

    def parser(self):
        def node():
            return {
                "value": 0,
                "total": 0,
                "parent": None,
                "children": [],
            }

        self.graph = defaultdict(node)

        for order in self.input:
            if "->" in order:
                root, children = order.split("->")
                children = children.split(",")
            else:
                root = order
                children = None

            program, value = root.split()
            value = int(value.strip("()"))
            self.graph[program]["value"] = value
            self.graph[program]["total"] = value

            if children:
                for child in children:
                    child_name = child.strip()
                    self.graph[child_name]["parent"] = program
                    self.graph[program]["children"].append(child_name)

    def part_one(self):
        program = next(iter(self.graph.keys()))
        while True:
            parent = self.graph[program]["parent"]
            if parent:
                program = parent
            else:
                break
        return program

    def get_totals(self, parent):
        children = self.graph[parent]["children"]
        return [self.graph[_]["total"] for _ in children]

    def are_equals(self, parent):
        totals = self.get_totals(parent)
        return len(Counter(totals)) == 1

    def part_two(self):
        # Compute totals
        for program in self.graph.keys():
            value = self.graph[program]["value"]
            while True:
                parent = self.graph[program]["parent"]
                if parent:
                    self.graph[parent]["total"] += value
                    program = parent
                else:
                    break

        parent = self.part_one()
        while True:
            children = self.graph[parent]["children"]
            totals = self.get_totals(parent)
            counter_totals = Counter(totals)
            alone_value = counter_totals.most_common()[-1][0]
            alone_child = children[totals.index(alone_value)]
            if all([self.are_equals(_) for _ in children]):
                break
            else:
                parent = alone_child

        children.remove(alone_child)
        difference = self.graph[children[0]]["total"] - alone_value
        return self.graph[alone_child]["value"] + difference


if __name__ == "__main__":
    Soluce().solve()
