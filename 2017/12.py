"""--- Day 12: Digital Plumber ---"""

from adventofcode.puzzle import Puzzle

from random import choice


class Soluce(Puzzle):
    test_solutions = [
        [6],  # Part One
        [2],  # Part Two
    ]

    def parser(self):
        self.records = {}
        for record in self.input:
            program, pipes = record.split(" <-> ")
            self.records[program] = pipes.split(", ")

    def find_group(self, program):
        programs = [
            program,
        ]
        group = set(programs)
        while programs:
            program = programs.pop()
            for pipe in self.records[program]:
                if pipe not in group:
                    group.add(pipe)
                    programs.append(pipe)
        return group

    def part_one(self):
        group = self.find_group("0")
        return len(group)

    def part_two(self):
        groups = []
        while self.records:
            program = choice(list(self.records.keys()))
            group = self.find_group(program)
            groups.append(group)
            for pipe in group:
                del self.records[pipe]
        return len(groups)


if __name__ == "__main__":
    Soluce().solve()
