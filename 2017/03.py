"""--- Day 3: Spiral Memory ---"""

from adventofcode.puzzle import Puzzle

import math
from itertools import product


class Soluce(Puzzle):
    test_solutions = [
        [0, 3, 2, 31],  # Part One
        [],  # Part Two
    ]

    def parser(self):
        self.input = int(self.input)

    def part_one(self):
        if self.input == 1:
            return 0
        layer = math.ceil((math.ceil(math.sqrt(self.input)) - 1) / 2)
        index = self.input - (2 * layer - 1) ** 2
        side_len = 2 * layer
        lower = side_len * math.floor(index / side_len) + 1
        mid = lower + layer - 1
        dist = layer + abs(index - mid)
        return dist

    def part_two(self):
        neighbors = [real + img * 1j for real, img in product((-1, 0, 1), repeat=2)]
        position = 0j
        grid = {position: 1}
        direction = 1 + 0j

        side_len = 1
        while True:
            for _ in range(2):
                for __ in range(side_len):
                    new_value = 0
                    position += direction
                    for neighbor in neighbors:
                        new_value += grid.get(position + neighbor, 0)
                    if new_value > self.input:
                        return new_value
                    grid[position] = new_value
                direction *= 1j
            side_len += 1


if __name__ == "__main__":
    Soluce().solve()
