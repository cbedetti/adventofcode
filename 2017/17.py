"""--- Day 17: Spinlock ---"""

from adventofcode.puzzle import Puzzle

from collections import deque


class Soluce(Puzzle):
    test_solutions = [
        [638],  # Part One
        [],  # Part Two
    ]

    def parser(self):
        self.input = int(self.input)

    def spinlock(self, duration):
        memory = deque([0])
        for data in range(1, duration):
            memory.rotate(-self.input)
            memory.append(data)
        return memory

    def part_one(self):
        return self.spinlock(2018)[0]

    def part_two(self):
        memory = self.spinlock(50_000_001)
        return memory[memory.index(0) + 1]


if __name__ == "__main__":
    Soluce().solve()
