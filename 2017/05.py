"""--- Day 5: A Maze of Twisty Trampolines, All Alike ---"""

from adventofcode.puzzle import Puzzle


class Soluce(Puzzle):
    test_solutions = [
        [5],  # Part One
        [10],  # Part Two
    ]

    def parser(self):
        self.input = {n: int(_) for n, _ in enumerate(self.input)}

    def part_one(self, strange=False):
        position = 0
        step = 0
        while True:
            instruction = self.input.get(position)
            if instruction is None:
                break
            new_position = position + instruction

            offset = 1
            if strange:
                if instruction >= 3:
                    offset = -1
            self.input[position] += offset

            position = new_position
            step += 1
        return step

    def part_two(self):
        return self.part_one(strange=True)


if __name__ == "__main__":
    Soluce().solve()
