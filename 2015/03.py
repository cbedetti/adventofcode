"""--- Day 3: Perfectly Spherical Houses in a Vacuum ---"""

from adventofcode.puzzle import Puzzle

import numpy as np
from itertools import cycle


class SantaPath:
    def __init__(self, instructions):
        """Init"""
        self.instructions = instructions
        self.position = np.array([0, 0])
        self.position_robot = np.array([0, 0])
        self.path = set()
        self.path.add(tuple(self.position))

    def compute_part1(self):
        """Compute the path"""
        directions_dict = {"^": [1, 0], "v": [-1, 0], "<": [0, -1], ">": [0, 1]}
        for direction in self.instructions:
            self.position += directions_dict[direction]
            self.path.add(tuple(self.position))

    def compute_part2(self):
        """Compute the path"""
        directions_dict = {"^": [1, 0], "v": [-1, 0], "<": [0, -1], ">": [0, 1]}
        for direction, position in zip(
            self.instructions, cycle([self.position, self.position_robot])
        ):
            position += directions_dict[direction]
            self.path.add(tuple(position))


class Soluce(Puzzle):
    def part_one(self):
        sp = SantaPath(self.input)
        sp.compute_part1()
        return len(sp.path)

    def part_two(self):
        sp = SantaPath(self.input)
        sp.compute_part2()
        return len(sp.path)


if __name__ == "__main__":
    Soluce().solve()
