"""--- Day 6: Probably a Fire Hazard ---"""

from adventofcode.puzzle import Puzzle

import numpy as np


class Santa_Board:

    def __init__(self, shape, instructions):
        self.board = np.zeros(shape, dtype=int)
        self.instructions = instructions

    def apply_instructions_one(self):
        for instruction in self.instructions:
            instruction_list = instruction.split(" ")
            begin_row, begin_col = [int(_) for _ in instruction_list[-3].split(",")]
            end_row, end_col = [int(_) + 1 for _ in instruction_list[-1].split(",")]

            if instruction.startswith("turn on"):
                self.board[begin_row:end_row, begin_col:end_col] = 1

            elif instruction.startswith("turn off"):
                self.board[begin_row:end_row, begin_col:end_col] = 0

            elif instruction.startswith("toggle"):
                self.board[begin_row:end_row, begin_col:end_col] -= 1
                self.board[self.board == -1] = 1

    def apply_instructions_two(self):
        for instruction in self.instructions:
            instruction_list = instruction.split(" ")
            begin_row, begin_col = [int(_) for _ in instruction_list[-3].split(",")]
            end_row, end_col = [int(_) + 1 for _ in instruction_list[-1].split(",")]

            if instruction.startswith("turn on"):
                self.board[begin_row:end_row, begin_col:end_col] += 1

            elif instruction.startswith("turn off"):
                self.board[begin_row:end_row, begin_col:end_col] -= 1
                self.board[self.board < 0] = 0

            elif instruction.startswith("toggle"):
                self.board[begin_row:end_row, begin_col:end_col] += 2


class Soluce(Puzzle):
    def part_one(self):
        sb = Santa_Board((1000, 1000), self.input)
        sb.apply_instructions_one()
        self.board1 = sb.board
        return sb.board.sum()

    def part_two(self):
        sb = Santa_Board((1000, 1000), self.input)
        sb.apply_instructions_two()
        self.board2 = sb.board
        return sb.board.sum()

    def vizu(self):
        self.save_array(self.board1, "06_part1.png")
        self.save_array(self.board2, "06_part2.png")


if __name__ == "__main__":
    soluce = Soluce()
    soluce.solve()
    soluce.vizu()
