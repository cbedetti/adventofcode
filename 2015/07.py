"""--- Day 7: Some Assembly Required ---"""

from adventofcode.puzzle import Puzzle

from functools import lru_cache


class Circuit:

    def __init__(self, instructions, override=None):
        self.instructions = instructions
        if override:
            self.instructions["b"] = str(override)

    @lru_cache()
    def get_value(self, value):
        try:
            return int(value)
        except ValueError:
            pass

        command = self.instructions[value].split(" ")

        if "AND" in command:
            return self.get_value(command[0]) & self.get_value(command[2])

        elif "OR" in command:
            return self.get_value(command[0]) | self.get_value(command[2])

        elif "LSHIFT" in command:
            return self.get_value(command[0]) << self.get_value(command[2])

        elif "RSHIFT" in command:
            return self.get_value(command[0]) >> self.get_value(command[2])

        elif "NOT" in command:
            return ~self.get_value(command[1])

        else:
            return self.get_value(command[0])


class Soluce(Puzzle):
    def parser(self):
        self.instructions = {}
        for instruction in self.input:
            command, result = instruction.split("->")
            self.instructions[result.strip()] = command.strip()

    def part_one(self):
        circuit = Circuit(self.instructions)
        return circuit.get_value("a")

    def part_two(self):
        circuit = Circuit(self.instructions, 956)
        return circuit.get_value("a")


if __name__ == "__main__":
    Soluce().solve()
