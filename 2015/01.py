"""--- Day 1: Not Quite Lisp ---"""

from adventofcode.puzzle import Puzzle


class Soluce(Puzzle):

    def part_one(self):
        return self.input.count("(") - self.input.count(")")

    def part_two(self):
        floor = 0
        for position, instruction in enumerate(self.input):
            if instruction == "(":
                floor += 1
            elif instruction == ")":
                floor -= 1

            if floor == -1:
                return position + 1


if __name__ == "__main__":
    Soluce().solve()
