"""--- Day 8: Matchsticks ---"""

from adventofcode.puzzle import Puzzle

import re


class Soluce(Puzzle):
    test_solutions = [
        [12],  # Part One
        [19],  # Part Two
    ]

    def part_one(self):
        result = 0
        for code in self.input:
            result += len(code) - len(eval(code))
        return result

    def part_two(self):
        return sum([len(re.findall(r'[\\"]', _)) + 2 for _ in self.input])


if __name__ == "__main__":
    Soluce().solve()
