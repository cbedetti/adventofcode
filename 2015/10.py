"""--- Day 10: Elves Look, Elves Say ---"""

from adventofcode.puzzle import Puzzle

from itertools import groupby


def look_and_say(numbers):
    result = []
    for number, group in groupby(numbers):
        result.append(str(len(list(group))) + number)
    return "".join(result)


def look_and_say_conway(numbers, iteration):
    """
    https://www.youtube.com/watch?v=ea7lJkEhytA
    https://en.wikipedia.org/wiki/Look-and-say_sequence#Growth_in_length
    """
    alpha = 1.303577269034
    return len(numbers) * alpha**iteration


class Soluce(Puzzle):

    def part_one(self):
        for _ in range(40):
            self.input = look_and_say(self.input)
        return len(self.input)

    def part_two(self):
        for _ in range(50):
            self.input = look_and_say(self.input)
        return len(self.input)


if __name__ == "__main__":
    Soluce().solve()
