"""--- Day 2: I Was Told There Would Be No Math ---"""

from adventofcode.puzzle import Puzzle

import numpy as np
from itertools import combinations


def wrapping_paper(dimensions_line):
    dimensions = sorted(map(int, dimensions_line.split("x")))

    area = dimensions[0] * dimensions[1]
    for _ in combinations(dimensions, 2):
        area += 2 * np.prod(_)

    return area


def wrapping_ribbon(dimensions_line):
    dimensions = sorted(map(int, dimensions_line.split("x")))
    ribbon = 2 * dimensions[0] + 2 * dimensions[1] + np.prod(dimensions)
    return ribbon


class Soluce(Puzzle):
    def part_one(self):
        return sum(map(wrapping_paper, self.input))

    def part_two(self):
        return sum(map(wrapping_ribbon, self.input))


if __name__ == "__main__":
    Soluce().solve()
