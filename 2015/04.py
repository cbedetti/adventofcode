"""--- Day 4: The Ideal Stocking Stuffer ---"""

from adventofcode.puzzle import Puzzle

import hashlib


def find_hash(secret_key, seed):
    answer = 0
    while True:
        to_hash = secret_key + str(answer)
        result = hashlib.md5(to_hash.encode()).hexdigest()
        if result[: len(seed)] == seed:
            return answer
        answer += 1


class Soluce(Puzzle):
    def part_one(self):
        return find_hash(self.input, "00000")

    def part_two(self):
        return find_hash(self.input, "000000")


if __name__ == "__main__":
    Soluce().solve()
