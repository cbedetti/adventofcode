"""--- Day 9: All in a Single Night ---"""

from adventofcode.puzzle import Puzzle

import networkx as nx
from itertools import permutations


def compute_paths(data):
    space_map = nx.Graph()
    for line in data:
        node1, _, node2, _, weight = line.split(" ")
        space_map.add_edge(node1, node2, weight=int(weight))

    possible_path = set()
    for path in permutations(space_map.nodes):
        if path[::-1] not in possible_path:
            possible_path.add(path)

    paths_len = []
    for path in possible_path:
        path_len = 0
        for node1, node2 in zip(path[1:], path[:-1]):
            path_len += space_map.get_edge_data(node1, node2)["weight"]
        paths_len.append(path_len)

    return paths_len


class Soluce(Puzzle):
    test_solutions = [
        [605],  # Part One
        [982],  # Part Two
    ]

    def part_one(self):
        return min(compute_paths(self.input))

    def part_two(self):
        return max(compute_paths(self.input))


if __name__ == "__main__":
    Soluce().solve()
