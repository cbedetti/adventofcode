"""--- Day 12: JSAbacusFramework.io ---"""

from adventofcode.puzzle import Puzzle

import re
import json


def iter_not_red(book):
    if isinstance(book, dict):
        if "red" in book.values():
            values = []

        else:
            values = book.values()

    elif isinstance(book, list):
        values = book

    for value in values:
        if isinstance(value, dict) or isinstance(value, list):
            yield from iter_not_red(value)

        elif isinstance(value, int):
            yield value


def sum_not_red(string):
    book = json.loads(string)
    book_list = list(iter_not_red(book))
    return sum(book_list)


class Soluce(Puzzle):
    test_solutions = [
        [6],  # Part One
        [6, 4, 0, 6],  # Part Two
    ]

    def part_one(self):
        return sum([int(_) for _ in re.findall(r"[-]?[0-9]+", self.input)])

    def part_two(self):
        return sum_not_red(self.input)


if __name__ == "__main__":
    Soluce().solve()
