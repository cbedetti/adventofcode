"""--- Day 5: Doesn't He Have Intern-Elves For This? ---"""

from adventofcode.puzzle import Puzzle

import re


def is_nice_part_one(string):
    if any(_ in string for _ in ["ab", "cd", "pq", "xy"]):
        return False

    double = False
    vowels = 0
    for first, second in zip(string[:-1], string[1:]):
        if first == second:
            double = True
        if first in "aeiou":
            vowels += 1
    if string[-1] in "aeiou":
        vowels += 1

    return all([double, vowels > 2])


def is_nice_part_two(string):
    return all(
        [
            re.search(r"(..).*\1", string),
            re.search(r"(.).\1", string),
        ]
    )


class Soluce(Puzzle):
    def count_nice(self, is_nice):
        nice_count = 0
        for string in self.input:
            if is_nice(string):
                nice_count += 1
        return nice_count

    def part_one(self):
        return self.count_nice(is_nice_part_one)

    def part_two(self):
        return self.count_nice(is_nice_part_two)


if __name__ == "__main__":
    Soluce().solve()
