"""--- Day 3: Squares With Three Sides ---"""

from adventofcode.puzzle import Puzzle

import numpy as np


class Soluce(Puzzle):

    def parser(self):
        self.triangles = [list(map(int, _.split())) for _ in self.input]

    def part_one(self):
        good_triangles = 0
        for sides in self.triangles:
            sides = sorted(sides)
            good_triangles += sides[0] + sides[1] > sides[2]
        return good_triangles

    def part_two(self):
        triangles = np.array(self.triangles).T.flatten()
        good_triangles = 0
        for sides in zip(*[iter(triangles)] * 3):
            sides = sorted(sides)
            good_triangles += sides[0] + sides[1] > sides[2]
        return good_triangles


if __name__ == "__main__":
    Soluce().solve()
