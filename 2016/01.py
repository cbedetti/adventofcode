"""--- Day 1: No Time for a Taxicab ---"""

from adventofcode.puzzle import Puzzle


class Soluce(Puzzle):
    test_solutions = [
        [5, 2, 12],  # Part One
        [None, None, None, 4],  # Part Two
    ]

    def parser(self):
        self.input = self.input.split(", ")

    def part_one(self):
        position = 0 + 0j
        direction = 0 + 1j

        for instruction in self.input:
            # Turning
            if instruction[0] == "L":
                direction *= 1j
            else:
                direction *= -1j

            # Moving
            position += int(instruction[1:]) * direction

        distance = int(abs(position.real) + abs(position.imag))
        return distance

    def part_two(self):
        position = 0 + 0j
        direction = 0 + 1j
        positions = set([position])

        for instruction in self.input:
            # Turning
            if instruction[0] == "L":
                direction *= 1j
            else:
                direction *= -1j

            # Moving
            for _ in range(int(instruction[1:])):
                position += direction
                if position in positions:
                    break
                positions.add(position)

            else:
                continue

            break

        distance = int(abs(position.real) + abs(position.imag))
        return distance


if __name__ == "__main__":
    Soluce().solve()
