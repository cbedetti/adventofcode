"""--- Day 12: Leonardo's Monorail ---"""

from adventofcode.puzzle import Puzzle


class Computer:
    def __init__(self, instructions):
        self.instructions = instructions
        self.pointer = 0
        self.register = [0] * 4
        self.register_dict = {"a": 0, "b": 1, "c": 2, "d": 3}
        self.commands_dict = {
            "cpy": self.cpy,
            "inc": self.inc,
            "dec": self.dec,
            "jnz": self.jnz,
        }

    def one_step(self, verbose=False):
        instruction = self.instructions[self.pointer]
        if isinstance(instruction, str):
            instruction = instruction.split()
            if instruction[0] == "cpy":
                # instruction[1] = int(instruction[1])
                instruction[2] = self.register_dict[instruction[2]]
            elif instruction[0] == "jnz":
                # instruction[1] = self.register_dict[instruction[1]]
                instruction[2] = int(instruction[2])
            else:
                instruction[1] = self.register_dict[instruction[1]]

            self.instructions[self.pointer] = instruction

        command = instruction[0]
        parameter = instruction[1:]
        self.commands_dict[command](*parameter)

        if verbose:
            print(self.register, self.pointer, self.instructions)

    def cpy(self, value, index):
        self.register[index] = self.get_value(value)
        self.pointer += 1

    def inc(self, index):
        self.register[index] += 1
        self.pointer += 1

    def dec(self, index):
        self.register[index] -= 1
        self.pointer += 1

    def jnz(self, value, jump):
        if self.get_value(value) != 0:
            self.pointer += jump
        else:
            self.pointer += 1

    def get_value(self, value):
        if value in "abcd":
            value = self.register[self.register_dict[value]]
        else:
            value = int(value)
        return value

    def debug(self, n):
        for _ in range(n - 1):
            self.one_step()
        self.one_step(True)

    def run(self):
        while 0 <= self.pointer < len(self.instructions):
            self.one_step()


class Soluce(Puzzle):
    test_solutions = [
        [42],  # Part One
        [],  # Part Two
    ]

    def part_one(self):
        computer = Computer(self.input)
        computer.run()
        return computer.register[0]

    def part_two(self):
        computer = Computer(self.input)
        computer.register[2] = 1
        computer.run()
        return computer.register[0]


if __name__ == "__main__":
    Soluce().solve()
