"""--- Day 6: Signals and Noise ---"""

from adventofcode.puzzle import Puzzle

from collections import Counter


class Soluce(Puzzle):
    test_solutions = [
        ["easter"],  # Part One
        ["advent"],  # Part Two
    ]

    def part_one(self):
        signal_len = len(self.input[0])
        columns = []
        for num in range(signal_len):
            columns.append([_[num] for _ in self.input])
        signal = [Counter(_).most_common(1)[0][0] for _ in columns]
        return "".join(signal)

    def part_two(self):
        signal_len = len(self.input[0])
        columns = []
        for num in range(signal_len):
            columns.append([_[num] for _ in self.input])
        signal = [Counter(_).most_common()[-1][0] for _ in columns]
        return "".join(signal)


if __name__ == "__main__":
    Soluce().solve()
