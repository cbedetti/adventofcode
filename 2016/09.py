"""--- Day 9: Explosives in Cyberspace ---"""

from adventofcode.puzzle import Puzzle

from collections import deque


def estimate(compressed, part):
    decompressed_len = 0
    while len(compressed) > 0:
        item = compressed.popleft()

        if item == " ":
            pass

        elif item == "(":
            marker = ""
            item = compressed.popleft()
            while item != ")":
                marker += item
                item = compressed.popleft()

            window_size, repeat = map(int, marker.split("x"))

            if part == "part_one":
                for _ in range(window_size):
                    compressed.popleft()
                decompressed_len += repeat * window_size

            elif part == "part_two":
                removed = deque([compressed.popleft() for _ in range(window_size)])
                decompressed_len += repeat * estimate(removed, "part_two")

        else:
            decompressed_len += 1

    return decompressed_len


class Soluce(Puzzle):
    test_solutions = [
        [6, 7, 9, 11, 6, 18],  # Part One
        [None, None, 9, None, None, 20, 241920, 445],  # Part Two
    ]

    def parser(self):
        self.file = deque(self.input)

    def part_one(self):
        return estimate(self.file, "part_one")

    def part_two(self):
        return estimate(self.file, "part_two")


if __name__ == "__main__":
    Soluce().solve()
