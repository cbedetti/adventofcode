"""--- Day 10: Balance Bots ---"""

from adventofcode.puzzle import Puzzle

from collections import defaultdict


class Usine:

    def __init__(self, instructions, comparing=set()):
        self.instructions = defaultdict(list)
        self.bots = defaultdict(set)
        self.output = {}
        self.comparing = comparing
        self.bot_comparing = None
        self.active_bots = []
        self.setup(instructions)

    def setup(self, instructions):
        for instruction in instructions:
            if "value" in instruction:
                ins = instruction.split()
                bot_number = ins[-1]
                microship = int(ins[1])

                microships = self.bots[bot_number]
                microships.add(microship)

                if len(microships) == 2:
                    self.active_bots.append(bot_number)

            elif "gives" in instruction:
                ins = instruction.split()
                bot_number = ins[1]
                low_instructions = ins[5:7]
                high_instructions = ins[-2:]

                self.instructions[bot_number] = [low_instructions, high_instructions]

    def run(self, searching_bot):
        while len(self.active_bots) > 0:
            active_bot = self.active_bots.pop()
            microships = self.bots[active_bot]

            if searching_bot and microships == self.comparing:
                self.bot_comparing = active_bot
                break

            microships = sorted(microships)

            for microship, (target, target_number) in zip(
                microships, self.instructions[active_bot]
            ):
                if target == "output":
                    self.output[target_number] = microship

                elif target == "bot":
                    self.bots[target_number].add(microship)
                    if len(self.bots[target_number]) == 2:
                        self.active_bots.append(target_number)

                self.bots[active_bot] = set()

    def part_two(self):
        return self.output["0"] * self.output["1"] * self.output["2"]


class Soluce(Puzzle):
    test_solutions = [
        ["2"],  # Part One
        [5 * 2 * 3],  # Part Two
    ]

    def part_one(self):
        if self.testing:
            comparing = set([2, 5])
        else:
            comparing = set([17, 61])

        usine = Usine(self.input, comparing)
        usine.run(searching_bot=True)
        return usine.bot_comparing

    def part_two(self):
        usine = Usine(self.input)
        usine.run(searching_bot=False)
        return usine.part_two()


if __name__ == "__main__":
    Soluce().solve()
