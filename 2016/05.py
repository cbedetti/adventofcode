"""--- Day 5: How About a Nice Game of Chess? ---"""

from adventofcode.puzzle import Puzzle

import hashlib


def find_pwd_one(door_id):
    answer = ""
    salt = 0
    while True:
        to_hash = door_id + str(salt)
        result = hashlib.md5(to_hash.encode()).hexdigest()
        if result.startswith("00000"):
            answer += result[5]

        if len(answer) == 8:
            return answer

        else:
            salt += 1


def find_pwd_two(door_id):
    answer = [None] * 8
    salt = 0
    while True:
        to_hash = door_id + str(salt)
        result = hashlib.md5(to_hash.encode()).hexdigest()
        if result.startswith("00000"):
            pos = result[5]
            if pos in map(str, range(8)):
                if not answer[int(pos)]:
                    answer[int(pos)] = result[6]

        if all(answer):
            return "".join(answer)

        else:
            salt += 1


class Soluce(Puzzle):
    test_solutions = [
        ["18f47a30"],  # Part One
        ["05ace8e3"],  # Part Two
    ]

    def part_one(self):
        return find_pwd_one(self.input)

    def part_two(self):
        return find_pwd_two(self.input)


if __name__ == "__main__":
    Soluce().solve()
