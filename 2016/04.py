"""--- Day 4: Security Through Obscurity ---"""

from adventofcode.puzzle import Puzzle

from string import ascii_lowercase
from collections import Counter


def is_real_room(encryption, checksum):
    counter = Counter(encryption.replace("-", ""))
    counter = sorted(counter.most_common(), key=lambda x: (-x[1], x[0]))
    computed_checksum = "".join(_[0] for _ in counter[:5])
    return computed_checksum == checksum


def caesar(plaintext, shift):
    shifted_alphabet = ascii_lowercase[shift:] + ascii_lowercase[:shift]
    table = str.maketrans(ascii_lowercase, shifted_alphabet)
    return plaintext.translate(table)


class Soluce(Puzzle):
    test_solutions = [
        [1514],  # Part One
        [],  # Part Two
    ]

    def parser(self):
        for input_string in self.input:
            rest, checksum = input_string[:-1].split("[")
            rest = rest.split("-")
            sector = int(rest[-1])
            encryption = "-".join(rest[:-1])
            yield encryption, sector, checksum

    def part_one(self):
        real_room = 0
        for encryption, sector, checksum in self.parser():
            if is_real_room(encryption, checksum):
                real_room += sector
        return real_room

    def part_two(self):
        for encryption, sector, checksum in self.parser():
            if is_real_room(encryption, checksum):
                if "north" in caesar(encryption, sector % 26):
                    return sector


if __name__ == "__main__":
    Soluce().solve()
