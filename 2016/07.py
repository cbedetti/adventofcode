"""--- Day 7: Internet Protocol Version 7 ---"""

from adventofcode.puzzle import Puzzle

import regex as re


def has_abba(string):
    return re.search(r"(.)(?!\1)(.)\2\1", string)


def supports_tls(ip):
    supernets = ip[0::2]
    hypernets = ip[1::2]
    if any(has_abba(_) for _ in hypernets):
        return False
    if any(has_abba(_) for _ in supernets):
        return True
    else:
        return False


def has_aba(string):
    return re.findall(r"(.)(?!\1)(.)\1", string, overlapped=True)


def supports_ssl(ip):
    supernets = ip[0::2]
    hypernets = ip[1::2]

    abas = set()
    for supernet in supernets:
        for aba in has_aba(supernet):
            abas.add(aba)

    for aba in abas:
        bab = f"{aba[1]}{aba[0]}{aba[1]}"
        if any(bab in hypernet for hypernet in hypernets):
            return True

    return False


class Soluce(Puzzle):
    test_solutions = [
        [2],  # Part One
        [None, 3],  # Part Two
    ]

    def parser(self):
        self.ips = [re.split(r"\[|\]", ip) for ip in self.input]

    def part_one(self):
        return sum(supports_tls(ip) for ip in self.ips)

    def part_two(self):
        return sum(supports_ssl(ip) for ip in self.ips)


if __name__ == "__main__":
    Soluce().solve()
