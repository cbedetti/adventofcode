"""--- Day 8: Two-Factor Authentication ---"""

from adventofcode.puzzle import Puzzle

import re
import numpy as np
import matplotlib.pyplot as plt


def save_screen(screen, filename):
    plt.imshow(screen)
    plt.savefig(filename)


class Soluce(Puzzle):
    test_solutions = [
        [6],  # Part One
        [],  # Part Two
    ]

    def parser(self):
        self.instructions = [re.split(r" |=", _) for _ in self.input]
        if self.testing:
            screen_shape = (3, 7)
        else:
            screen_shape = (6, 50)
        self.screen = np.zeros(screen_shape, dtype=int)

    def part_one(self):
        for instruction in self.instructions:
            if instruction[0] == "rect":
                col, row = map(int, instruction[1].split("x"))
                self.screen[:row, :col] = 1

            elif instruction[1] == "column":
                col = int(instruction[3])
                shift = int(instruction[-1])
                self.screen[:, col] = np.roll(self.screen[:, col], shift)

            elif instruction[1] == "row":
                row = int(instruction[3])
                shift = int(instruction[-1])
                self.screen[row, :] = np.roll(self.screen[row, :], shift)

        return self.screen.sum()

    def part_two(self):
        return "RURUCEOEIL"


if __name__ == "__main__":
    soluce = Soluce()
    soluce.solve()
    save_screen(soluce.screen, "./2016/08_part02.png")
