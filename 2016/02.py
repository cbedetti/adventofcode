"""--- Day 2: Bathroom Security ---"""

from adventofcode.puzzle import Puzzle

import numpy as np


class Soluce(Puzzle):
    test_solutions = [
        ["1985"],  # Part One
        ["5DB3"],  # Part Two
    ]

    def parser(self):
        self.keypad_one = np.array(
            [
                [None, None, None, None, None],
                [None, 1, 2, 3, None],
                [None, 4, 5, 6, None],
                [None, 7, 8, 9, None],
                [None, None, None, None, None],
            ]
        )
        self.keypad_two = np.array(
            [
                [None, None, None, None, None, None, None],
                [None, None, None, 1, None, None, None],
                [None, None, 2, 3, 4, None, None],
                [None, 5, 6, 7, 8, 9, None],
                [None, None, "A", "B", "C", None, None],
                [None, None, None, "D", None, None, None],
                [None, None, None, None, None, None, None],
            ]
        )
        self.moves = {
            "L": -1j,
            "R": 1j,
            "U": -1,
            "D": 1,
        }

    def apply_instructions(self, keypad, position):
        code = ""
        current_code = 5

        for instructions in self.input:
            for instruction in instructions:
                new_position = position + self.moves[instruction]

                new_code = keypad[int(new_position.real), int(new_position.imag)]
                if new_code:
                    position = new_position
                    current_code = new_code

            code += str(current_code)

        return code

    def part_one(self):
        return self.apply_instructions(self.keypad_one, 2 + 2j)

    def part_two(self):
        return self.apply_instructions(self.keypad_two, 3 + 1j)


if __name__ == "__main__":
    Soluce().solve()
