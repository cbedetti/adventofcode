"""--- Day 13: A Maze of Twisty Little Cubicles ---"""

from adventofcode.puzzle import Puzzle

import numpy as np
from collections import Counter


class Cubicles:
    def __init__(self, favorite_number, start, target=None) -> None:
        self.favorite_number = favorite_number
        self.start = np.array(start)
        self.visited = [tuple(self.start)]
        self.reach = 1
        self.queue = [self.start]
        self.target = target
        self.steps = 0
        self.adjacents = [(1, 0), (-1, 0), (0, 1), (0, -1)]
        self.searching = False

    def coord_state(self, coord):
        states = [".", "#"]
        x, y = coord
        if x < 0 or y < 0:
            return None
        value = x * x + 3 * x + 2 * x * y + y + y * y + self.favorite_number
        return states[Counter(bin(value)[2:])["1"] % 2]

    def run_part_one(self):
        self.searching = True
        while self.searching:
            self.run_one_step(True)

    def run_one_step(self, searching=False):
        self.steps += 1
        next_queue = []
        for cubicle in self.queue:
            for vector in self.adjacents:
                adjacent = cubicle + vector
                if searching and np.array_equal(adjacent, self.target):
                    self.searching = False
                    break

                elif tuple(adjacent) in self.visited:
                    continue

                elif self.coord_state(adjacent) == ".":
                    next_queue.append(adjacent)
                    self.reach += 1

                self.visited.append(tuple(adjacent))

        self.queue = next_queue


class Soluce(Puzzle):
    test_solutions = [
        [11],  # Part One
        [9],  # Part Two
    ]

    def parser(self):
        self.favorite_number = int(self.input)

    def part_one(self):
        if self.testing:
            target = [7, 4]
        else:
            target = [31, 39]
        cubicles = Cubicles(self.favorite_number, [1, 1], target)
        cubicles.run_part_one()
        return cubicles.steps

    def part_two(self):
        if self.testing:
            steps = 4
        else:
            steps = 50
        cubicles = Cubicles(self.favorite_number, [1, 1])
        for _ in range(steps):
            cubicles.run_one_step()
        return cubicles.reach


if __name__ == "__main__":
    Soluce().solve()
