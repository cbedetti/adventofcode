"""--- Day 2: Red-Nosed Reports ---"""

from adventofcode.puzzle import Puzzle


def is_safe(report):
    variations = [num1 - num2 for num1, num2 in zip(report, report[1:])]
    if 0 in variations:
        return False
    elif all(-3 <= _ < 0 for _ in variations):
        return True
    elif all(0 < _ <= 3 for _ in variations):
        return True
    else:
        return False


def dampener(report):
    for num in range(len(report)):
        report_dampenered = report.copy()
        report_dampenered.pop(num)
        yield report_dampenered


class Soluce(Puzzle):
    test_solutions = [
        [2],  # Part One
        [4],  # Part Two
    ]

    def parser(self):
        self.reports = [[*map(int, _.split())] for _ in self.input]

    def part_one(self):
        return sum(map(is_safe, self.reports))

    def part_two(self):
        safe_report = 0
        for report in self.reports:
            if is_safe(report):
                safe_report += 1
            else:
                if any(is_safe(_) for _ in dampener(report)):
                    safe_report += 1
        return safe_report


if __name__ == "__main__":
    Soluce().solve()
