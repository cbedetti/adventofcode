"""--- Day 7: Bridge Repair ---"""

from adventofcode.puzzle import Puzzle

import re
from itertools import product


def is_good_operators(test, equation, operators):
    result = equation[0]
    for element, operator in zip(equation[1:], operators):
        if result > test:
            return False
        else:
            if operator == "+":
                result += element
            elif operator == "*":
                result *= element
            elif operator == "|":
                result = int(f"{result}{element}")
    return result == test


def good_calibration(calibration, possible_calibrations):
    test, equation = calibration[0], calibration[1:]
    if any(
        is_good_operators(test, equation, operators)
        for operators in product(possible_calibrations, repeat=len(equation) - 1)
    ):
        return test
    return 0


class Soluce(Puzzle):
    test_solutions = [
        [3749],  # Part One
        [11387],  # Part Two
    ]

    def parser(self):
        self.input = [list(map(int, re.findall(r"\d+", line))) for line in self.input]

    def part_one(self):
        return sum(good_calibration(calibration, "+*") for calibration in self.input)

    def part_two(self):
        return sum(good_calibration(calibration, "+*|") for calibration in self.input)


if __name__ == "__main__":
    Soluce().solve()
