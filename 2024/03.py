"""--- Day 3: Mull It Over ---"""

from adventofcode.puzzle import Puzzle

import re


class Soluce(Puzzle):
    test_solutions = [
        [161],  # Part One
        [None, 48],  # Part Two
    ]

    def parser(self):
        if isinstance(self.input, list):
            self.input = "".join(self.input)

    def part_one(self):
        multiplications = 0
        for mul in re.findall(r"mul\(\d{1,3},\d{1,3}\)", self.input):
            a, b = map(int, re.findall(r"\d+", mul))
            multiplications += a * b
        return multiplications

    def part_two(self):
        commands = list(re.finditer(r"mul\(\d{1,3},\d{1,3}\)", self.input))
        commands += list(re.finditer(r"do\(\)", self.input))
        commands += list(re.finditer(r"don't\(\)", self.input))
        commands = sorted(commands, key=lambda x: x.span()[0])

        multiplications = 0
        enable = True
        for command in commands:
            instruction = command.group()
            if instruction.startswith("don"):
                enable = False

            elif instruction.startswith("do"):
                enable = True

            elif enable:
                a, b = map(int, re.findall(r"\d+", instruction))
                multiplications += a * b
        return multiplications


if __name__ == "__main__":
    Soluce().solve()
