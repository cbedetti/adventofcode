"""--- Day 1: Historian Hysteria ---"""

from adventofcode.puzzle import Puzzle

import numpy as np
from collections import Counter


class Soluce(Puzzle):
    test_solutions = [
        [11],  # Part One
        [31],  # Part Two
    ]

    def parser(self):
        self.input = np.array([_.split() for _ in self.input], dtype=int)
        self.left, self.right = self.input[:, 0], self.input[:, 1]

    def part_one(self):
        return sum(np.abs(np.sort(self.left) - np.sort(self.right)))

    def part_two(self):
        right_counter = Counter(self.right)
        return sum(
            occurence * id * right_counter[id]
            for id, occurence in Counter(self.left).items()
        )


if __name__ == "__main__":
    Soluce().solve()
