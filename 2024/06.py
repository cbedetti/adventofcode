"""--- Day 6: Guard Gallivant ---"""

from adventofcode.puzzle import Puzzle


def path_of_the_guard(position, grid):
    direction = -1j
    state = (position, direction)
    visited = set()
    states = set()
    while True:
        visited.add(position)
        states.add(state)

        new_position = position + direction
        element = grid.get(new_position)
        if element == "#":
            direction *= 1j
        elif element == ".":
            position = new_position
        else:
            break

        new_state = (position, direction)
        if new_state in states:
            return "loop"
        else:
            state = new_state

    return visited


class Soluce(Puzzle):
    test_solutions = [
        [41],  # Part One
        [6],  # Part Two
    ]

    def parser(self):
        self.grid = {}
        for imag, line in enumerate(self.input):
            for real, element in enumerate(line):
                position = real + imag * 1j
                if element == "^":
                    self.guard = position
                    self.grid[position] = "."
                else:
                    self.grid[position] = element

    def part_one(self):
        return len(path_of_the_guard(self.guard, self.grid))

    def part_two(self):
        visited = path_of_the_guard(self.guard, self.grid)
        visited.remove(self.guard)
        loop = 0
        for new_obstruction in visited:
            grid = self.grid.copy()
            grid[new_obstruction] = "#"
            if path_of_the_guard(self.guard, grid) == "loop":
                loop += 1
        return loop


if __name__ == "__main__":
    Soluce().solve()
