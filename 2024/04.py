"""--- Day 4: Ceres Search ---"""

from adventofcode.puzzle import Puzzle

from itertools import product


class Soluce(Puzzle):
    test_solutions = [
        [18],  # Part One
        [9],  # Part Two
    ]

    def parser(self):
        self.grid = {}
        self.input_one = []
        self.input_two = []
        for imag, line in enumerate(self.input):
            for real, data in enumerate(line):
                position = real + imag * 1j
                self.grid[position] = data
                if data == "X":
                    self.input_one.append(position)
                elif data == "A":
                    self.input_two.append(position)

    def part_one(self):
        xmas = 0
        neighbors = [real + img * 1j for real, img in product((-1, 0, 1), repeat=2)]
        for position in self.input_one:
            for neighbor in neighbors:
                searching = "".join(
                    self.grid.get(position + num * neighbor, "") for num in range(1, 4)
                )
                if searching == "MAS":
                    xmas += 1
        return xmas

    def part_two(self):
        xmas = 0
        neighbors = [-1 - 1j, 1 - 1j, 1 + 1j, -1 + 1j]
        for position in self.input_two:
            searching = ""
            for neighbor in neighbors:
                searching += self.grid.get(position + neighbor, "")
            if searching in ["MSSM", "SSMM", "SMMS", "MMSS"]:
                xmas += 1
        return xmas


if __name__ == "__main__":
    Soluce().solve()
