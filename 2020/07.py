"""--- Day 7: Handy Haversacks ---"""

from adventofcode.puzzle import Puzzle

import networkx as nx
import matplotlib.pyplot as plt


def get_successors(graph, parent, parent_weight):
    successors = []
    for node in graph.successors(parent):
        successors.append((node, graph[parent][node]["weight"] * parent_weight))
    return successors


class Soluce(Puzzle):
    test_solutions = [
        [4],  # Part One
        [32, 126],  # Part Two
    ]

    def parser(self):
        self.data = []
        for line in self.input:
            container, containees = line.split("contain")
            container = container[:-6]
            for containee in containees.split(","):
                if not containee == " no other bags.":
                    __, weight, adj, color, ___ = containee.split(" ")
                    self.data.append([container, " ".join([adj, color]), int(weight)])

    def part_one(self):
        graph = nx.DiGraph()
        for container, containee, weight in self.data:
            graph.add_edge(containee, container, weight=weight)
        colors = set()
        containers = list(graph.adj["shiny gold"])
        while len(containers) > 0:
            container = containers.pop()
            colors.add(container)
            containers += list(graph.adj[container])
        return len(colors)

    def part_two(self, bag_of_interest="shiny gold"):
        graph = nx.DiGraph()
        for container, containee, weight in self.data:
            graph.add_edge(container, containee, weight=weight)
        bags = 0
        successors = get_successors(graph, bag_of_interest, 1)
        while len(successors) > 0:
            parent, weight = successors.pop()
            parent_successors = get_successors(graph, parent, weight)
            bags += weight
            if parent_successors:
                successors += parent_successors
        return bags

    def plot(self):
        graph = nx.DiGraph()
        for container, containee, weight in self.data:
            graph.add_edge(container, containee, weight=weight)

        result = []
        for bag in graph.nodes:
            bag_count = self.part_two(bag)
            if bag_count:
                result.append(bag_count)
        result.sort()
        plt.plot(result)
        plt.title(
            "How many bags a bag can hold?"
            + f"\n{len(graph.nodes) - len(result)} empty bags"
        )
        plt.xlabel("Types of bags (sorted by capacity)")
        plt.ylabel("Many many bags (log)")
        plt.yscale("log")
        plt.xticks([], [])
        plt.grid(True)
        # Biggest bag, muted brown with 12,543,243,007 bags
        plt.savefig("./2020/07_bags.png")


if __name__ == "__main__":
    soluce = Soluce()
    soluce.solve()
    # soluce.plot()
