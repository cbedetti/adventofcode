"""--- Day 6: Custom Customs ---"""

from adventofcode.puzzle import Puzzle


class Soluce(Puzzle):
    test_solutions = [
        [11],  # Part One
        [6],  # Part Two
    ]

    def parser(self):
        self.data = [[]]
        for line in self.input:
            if line:
                self.data[-1].append(line)
            else:
                self.data.append([])

    def part_one(self):
        return sum([len(set(list("".join(_)))) for _ in self.data])

    def part_two(self):
        result = 0
        for group in self.data:
            questions = set(list(group[0]))
            for question in group[1:]:
                questions.intersection_update(list(question))
            result += len(questions)
        return result


if __name__ == "__main__":
    Soluce().solve()
