"""--- Day 2: Password Philosophy ---"""

from adventofcode.puzzle import Puzzle

from collections import Counter


class Soluce(Puzzle):
    test_solutions = [
        [2],  # Part One
        [1],  # Part Two
    ]

    def parser(self):
        self.data = []
        for line in self.input:
            line = line.rstrip().split(" ")
            new_line = [int(_) for _ in line[0].split("-")]
            new_line += [line[1][0], line[2]]
            self.data.append(new_line)

    def part_one(self):
        good_password = 0
        for minimum, maximum, letter, password in self.data:
            counter = Counter(password)
            good_password += minimum <= counter[letter] <= maximum
        return good_password

    def part_two(self):
        good_password = 0
        for mini, maxi, letter, password in self.data:
            good_password += (password[mini - 1] == letter) ^ (
                password[maxi - 1] == letter
            )
        return good_password


if __name__ == "__main__":
    Soluce().solve()
