"""--- Day 1: Report Repair ---"""

from adventofcode.puzzle import Puzzle

import numpy as np
from itertools import combinations


class Soluce(Puzzle):
    def parser(self):
        self.input = map(int, self.input)

    def part_one(self):
        for comb in combinations(self.input, 2):
            if sum(comb) == 2020:
                return np.prod(comb)

    def part_two(self):
        for comb in combinations(self.input, 3):
            if sum(comb) == 2020:
                return np.prod(comb)


if __name__ == "__main__":
    Soluce().solve()
