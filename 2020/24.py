"""--- Day 24: Lobby Layout ---"""

import adventofcode as aoc

import numpy as np
from collections import Counter


def read_input(test_tag=""):
    input_path = aoc.input_path(2020, 24, test_tag)
    data = []
    with input_path.open("r") as input_file:
        for line in input_file:
            data.append(iter(line.rstrip()))
    return data


DIRECTIONS = {
    "nw": np.array([-1, 0]),
    "se": np.array([1, 0]),
    "ne": np.array([-1, 1]),
    "sw": np.array([1, -1]),
    "e": np.array([0, 1]),
    "w": np.array([0, -1]),
}


def compute_black_tiles(data):
    black_tiles = set()
    for instruction in data:
        active_tile = np.array([0, 0])
        while True:
            try:
                direction = next(instruction)
                if direction in "ns":
                    direction += next(instruction)
                active_tile += DIRECTIONS[direction]
            except StopIteration:
                active_tile = tuple(active_tile)
                if active_tile in black_tiles:
                    black_tiles.remove(active_tile)
                else:
                    black_tiles.add(active_tile)
                break
    return black_tiles


def part_one(test=""):
    data = read_input(test)
    return len(compute_black_tiles(data))


def run_generation(black_tiles):
    white_tiles = Counter()
    black_tiles_to_remove = []
    for black_tile in black_tiles:
        adjacent = 0
        for direction in DIRECTIONS.values():
            tile = tuple(black_tile + direction)
            if tile in black_tiles:
                adjacent += 1
            else:
                white_tiles[tile] += 1

        if adjacent == 0 or adjacent > 2:
            black_tiles_to_remove.append(black_tile)

    for tile in black_tiles_to_remove:
        black_tiles.remove(tile)

    for tile, adjacent in white_tiles.items():
        if adjacent == 2:
            black_tiles.add(tile)

    return black_tiles


def part_two(test=""):
    data = read_input(test)
    black_tiles = compute_black_tiles(data)
    for _ in range(100):
        black_tiles = run_generation(black_tiles)
    return len(black_tiles)


if __name__ == "__main__":
    # Part One
    assert part_one("test01") == 10
    print(f"Part one answer: {part_one()}")  # 287

    # Part Two
    assert part_two("test01") == 2208
    print(f"Part two answer: {part_two()}")  # 3636

    # Timers
    aoc.timer("part_one()", 100, 5, globals())  # 100 loops, best of 5: 12 ms
    aoc.timer("part_two()", 10, 1, globals())  # 10 loops, best of 1: 5.9 s
