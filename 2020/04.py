"""--- Day 4: Passport Processing ---"""

from adventofcode.puzzle import Puzzle

import re
from collections import defaultdict


class Soluce(Puzzle):
    test_solutions = [
        [2],  # Part One
        [None, 0, 4],  # Part Two
    ]

    def parser(self):
        self.data = []
        passport = defaultdict(lambda: "")
        for line in self.input:
            if line:
                for key, value in [kv.split(":") for kv in line.split(" ")]:
                    passport[key] = value
            else:
                self.data.append(passport)
                passport = defaultdict(lambda: "")
        self.data.append(passport)

    def part_one(self):
        required_fields = set(["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"])
        valid_passport = 0
        for passport in self.data:
            valid_passport += required_fields.issubset(passport.keys())
        return valid_passport

    def part_two(self):
        valid_passport = 0
        for passport in self.data:
            try:
                byr_test = 1920 <= int(passport["byr"]) <= 2002
            except ValueError:
                byr_test = False

            try:
                iyr_test = 2010 <= int(passport["iyr"]) <= 2020
            except ValueError:
                iyr_test = False

            try:
                eyr_test = 2020 <= int(passport["eyr"]) <= 2030
            except ValueError:
                eyr_test = False

            try:
                hgt = passport["hgt"]
                if hgt[-2:] == "cm":
                    hgt_test = 150 <= int(hgt[:-2]) <= 193
                elif hgt[-2:] == "in":
                    hgt_test = 59 <= int(hgt[:-2]) <= 76
                else:
                    hgt_test = False
            except ValueError:
                hgt_test = False

            valid_passport += all(
                [
                    byr_test,
                    iyr_test,
                    eyr_test,
                    hgt_test,
                    re.match("^#[0-9a-f]{6}$", passport["hcl"]),
                    re.match(
                        "^amb$|^blu$|^brn$|^gry$|^grn$|^hzl$|^oth$", passport["ecl"]
                    ),
                    re.match("^[0-9]{9}$", passport["pid"]),
                ]
            )
        return valid_passport


if __name__ == "__main__":
    Soluce().solve()
