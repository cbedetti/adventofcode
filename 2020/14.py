"""--- Day 14: Docking Data ---"""

import adventofcode as aoc

from itertools import product


def read_input(test_tag=""):
    input_path = aoc.input_path(2020, 14, test_tag)
    data = []
    with input_path.open("r") as input_file:
        for line in input_file:
            data.append(line.rstrip())
    return data


def mask_dict(line):
    mask = {"0": [], "1": [], "X": []}
    for index, bit in enumerate(line.split(" ")[-1]):
        if bit == "0":
            mask[bit].append(index)
        elif bit == "1":
            mask[bit].append(index)
        else:
            mask["X"].append(index)
    return mask


def decode_value(value, mask):
    for bitshift in ["0", "1"]:
        for index in mask[bitshift]:
            value[index] = bitshift
    return value


def decode_adress(adress, mask):
    for index in mask["1"]:
        adress[index] = "1"
    for bitshifts in product("01", repeat=len(mask["X"])):
        for index, bitshift in zip(mask["X"], bitshifts):
            adress[index] = bitshift
        yield "".join(adress)


def part_one(test=""):
    data = read_input(test)
    mem = {}
    for line in data:
        if line.startswith("mask"):
            mask = mask_dict(line)
        else:
            adress = line.split("[")[1].split("]")[0]
            value = list(format(int(line.split("=")[1]), "036b"))
            mem[adress] = decode_value(value, mask)
    return sum([int("".join(_), 2) for _ in mem.values()])


def part_two(test=""):
    data = read_input(test)
    mem = {}
    for line in data:
        if line.startswith("mask"):
            mask = mask_dict(line)
        else:
            adress_raw = list(format(int(line.split("[")[1].split("]")[0]), "036b"))
            value = int(line.split("=")[1])
            for adress in decode_adress(adress_raw, mask):
                mem[adress] = value
    return sum(mem.values())


if __name__ == "__main__":
    # Tests
    assert part_one("test01") == 165
    assert part_two("test02") == 208

    # Answers
    print(f"Part one answer: {part_one()}")  # 11884151942312
    print(f"Part two answer: {part_two()}")  # 2625449018811

    # Timers
    aoc.timer("part_one()", 100, 5, globals())  # 100 loops, best of 5: 3.5 ms per loop
    aoc.timer("part_two()", 100, 5, globals())  # 100 loops, best of 5: 130 ms per loop
