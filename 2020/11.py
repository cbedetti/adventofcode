"""--- Day 11: Seating System ---"""

import adventofcode as aoc

import numpy as np
from itertools import product
from collections import Counter


def read_input(test_tag=""):
    input_path = aoc.input_path(2020, 11, test_tag)
    data = []
    with input_path.open("r") as input_file:
        for line in input_file:
            data.append(list(line.rstrip()))
    return np.array(data, dtype=str)


class Seat:

    def __init__(self, coordinates, threshold):
        self.coordinates = coordinates
        self.threshold = threshold
        self.adjacents = []
        self.occupied = True
        self.occupied_next = True
        self.inactive = False

    def __str__(self):
        if self.occupied:
            return "#"
        else:
            return "L"

    def set_adjacents(self, data, visual):
        if visual:
            self.set_adjacents_visual(data)
        else:
            self.set_adjacents_close(data)

    def set_adjacents_close(self, data):
        shifts_coordinates = [range(_ - 1, _ + 2) for _ in self.coordinates]
        for shifts in product(*shifts_coordinates):
            if -1 in shifts:
                continue
            try:
                if data[shifts] == "L" and shifts != self.coordinates:
                    self.adjacents.append(shifts)
            except IndexError:
                pass
        self.set_inactive()

    def set_adjacents_visual(self, data):
        shifts_coordinates = [range(-1, 2) for _ in self.coordinates]
        for shifts in product(*shifts_coordinates):
            if shifts == (0, 0):
                continue
            looking = True
            looking_index = np.add(self.coordinates, shifts)
            while looking:
                if -1 in looking_index:
                    looking = False
                else:
                    try:
                        if data[tuple(looking_index)] == "L":
                            self.adjacents.append(tuple(looking_index))
                            looking = False
                        else:
                            looking_index += shifts
                    except IndexError:
                        looking = False
        self.set_inactive()

    def set_inactive(self):
        if len(self.adjacents) < self.threshold:
            self.inactive = True

    def compute_next_state(self, adjacents):
        if self.occupied and adjacents >= self.threshold:
            self.occupied_next = False
        elif not self.occupied and adjacents == 0:
            self.occupied_next = True

    def apply_generation(self):
        self.occupied = self.occupied_next


class WaitingRoom:

    def __init__(self, data, visual=False, threshold=4):
        self.generation = 1
        self.data = data
        self.stabilized = False
        self.seats = {}
        self.active_seats = []
        for index, floor in np.ndenumerate(data):
            if floor == "L":
                seat = Seat(index, threshold)
                seat.set_adjacents(data, visual)
                if not seat.inactive:
                    self.active_seats.append(index)
                self.seats[index] = seat

    def __str__(self):
        room = np.full_like(self.data, ".")
        for index, seat in self.seats.items():
            room[index] = str(seat)
        result = "\n".join(["".join(_) for _ in room])
        return result

    def run_generation(self):
        # Compute next state
        for index in self.active_seats:
            adjacents = sum(
                [self.seats[_].occupied for _ in self.seats[index].adjacents]
            )
            self.seats[index].compute_next_state(adjacents)

        # Apply next state
        state_before = str(self)
        for seat in self.seats.values():
            seat.apply_generation()
        state_after = str(self)
        self.generation += 1

        if state_after == state_before:
            self.stabilized = True

    def run_generations(self, n=1):
        for _ in range(n):
            self.run_generation()

    @property
    def seats_occupied(self):
        return Counter(str(self))["#"]


def part_one(test=""):
    room = WaitingRoom(read_input(test))
    while not room.stabilized:
        room.run_generation()
    return room.seats_occupied


def part_two(test=""):
    room = WaitingRoom(read_input(test), True, 5)
    while not room.stabilized:
        room.run_generation()
    return room.seats_occupied


if __name__ == "__main__":
    # Tests
    assert part_one("test01") == 37
    assert part_two("test01") == 26

    # Answers
    print(f"Part one answer: {part_one()}")  # 2261
    print(f"Part two answer: {part_two()}")  # 2039

    # Timers
    aoc.timer("part_one()", 100, 5, globals())  # 100 loops, best of 5: 3.1 s per loop
    aoc.timer("part_two()", 100, 5, globals())  # 100 loops, best of 5: 6.3 s per loop
