"""--- Day 5: Binary Boarding ---"""

from adventofcode.puzzle import Puzzle

from itertools import product


class Soluce(Puzzle):

    def parser(self):
        self.data = []
        for line in self.input:
            for letter, binary in [("B", "1"), ("F", "0"), ("R", "1"), ("L", "0")]:
                line = line.replace(letter, binary)
            self.data.append(tuple([int(line[:-3], 2), int(line[-3:], 2)]))

    def part_one(self):
        return max(_[0] * 8 + _[1] for _ in self.data)

    def part_two(self):
        first_seat_found = False
        for seat in product(range(1, 127), range(8)):
            if first_seat_found:
                if seat not in self.data:
                    return seat[0] * 8 + seat[1]
            else:
                if seat in self.data:
                    first_seat_found = True


if __name__ == "__main__":
    Soluce().solve()
