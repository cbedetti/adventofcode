"""--- Day 10: Adapter Array ---"""

import adventofcode as aoc

import numpy as np
from collections import Counter
from functools import lru_cache


def read_input(test_tag=""):
    input_path = aoc.input_path(2020, 10, test_tag)
    data = []
    with input_path.open("r") as input_file:
        for line in input_file:
            data.append(int(line.rstrip()))
    data.sort()
    return data


def part_one(test=""):
    data = read_input(test)
    differences = Counter([3])
    previous_jolt = 0
    for jolt in data:
        differences[jolt - previous_jolt] += 1
        previous_jolt = jolt
    return differences[1] * differences[3]


@lru_cache()
def find_jolting(n):
    """
    https://mathworld.wolfram.com/TribonacciNumber.html

    3: (0, 1, 2):       [0, 1, 2], [0, 2]
    4: (0, 1, 2, 3):    [0, 1, 2, 3], [0, 1, 3], [0, 2, 3], [0, 3]
    5: (0, 1, 2, 3, 4): [0, 1, 2, 3, 4], [0, 1, 2, 4], [0, 1, 3, 4],
                        [0, 2, 3, 4], [0, 1, 4], [0, 2, 4], [0, 3, 4]
    """
    if n < 3:
        return {0: 0, 1: 1, 2: 1}[n]
    else:
        return find_jolting(n - 1) + find_jolting(n - 2) + find_jolting(n - 3)


def part_two(test=""):
    data = read_input(test)
    ones_len = 1
    joltings = []
    for jolt in [0] + data:
        if (jolt + 1) in data:
            ones_len += 1
        else:
            if ones_len > 2:
                joltings.append(find_jolting(ones_len))
            ones_len = 1
    return np.product(joltings)


if __name__ == "__main__":
    # Tests
    assert part_one("test01") == 35
    assert part_one("test02") == 220
    assert part_two("test01") == 8
    assert part_two("test02") == 19208

    # Answers
    print(f"Part one answer: {part_one()}")  # 1856
    print(f"Part two answer: {part_two()}")  # 2314037239808

    # Timers
    aoc.timer("part_one()", 1000, 5, globals())  # 1000 loops, best of 5: 0.18 ms
    aoc.timer("part_two()", 1000, 5, globals())  # 1000 loops, best of 5: 0.27 ms
