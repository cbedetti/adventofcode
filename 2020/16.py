"""--- Day 16: Ticket Translation ---"""

import adventofcode as aoc

import numpy as np


def read_input(test_tag=""):
    input_path = aoc.input_path(2020, 16, test_tag)
    rules = {}
    tickets = []
    with input_path.open("r") as input_file:
        for line in input_file:
            if "-" in line:
                key, ranges = line.split(":")
                ranges = ranges.split(" ")
                values = set(
                    range(*[int(_) + idx for idx, _ in enumerate(ranges[1].split("-"))])
                )  # noqa
                values.update(
                    range(*[int(_) + idx for idx, _ in enumerate(ranges[3].split("-"))])
                )  # noqa
                rules[key] = values
            elif "," in line:
                tickets.append(line.split(","))
    tickets = np.array(tickets, dtype=int)
    return rules, tickets[0], tickets[1:]


def part_one(test=""):
    rules, _, tickets = read_input(test)

    good_values = set()
    for values in rules.values():
        good_values.update(values)

    error_mask = ~np.isin(tickets, list(good_values))
    return tickets[error_mask].sum()


def part_two(test=""):
    rules, my_ticket, tickets = read_input(test)

    good_values = set()
    for values in rules.values():
        good_values.update(values)

    mask = np.all(np.isin(tickets, list(good_values)), axis=1)
    good_tickets = tickets[mask]

    links = []
    for key, values in rules.items():
        mask = np.isin(good_tickets, list(values))
        links.append(np.all(mask, axis=0))
    links = np.array(links)

    while np.sum(links) != len(my_ticket):
        reduced_rules = (np.sum(links, axis=1) == 1).nonzero()[0]
        for reduced_rule in reduced_rules:
            reduced_col = links[reduced_rule].nonzero()[0]
            links[:, reduced_col] = np.full_like(links[:, reduced_col], False)
            links[reduced_rule, reduced_col] = True

    result = 1
    for rule_coord, key in zip(np.argwhere(links), rules.keys()):
        if key.startswith("departure"):
            result *= my_ticket[rule_coord[1]]
    return result


if __name__ == "__main__":
    # Tests
    assert part_one("test01") == 71
    assert part_two("test02") == 156

    # Answers
    print(f"Part one answer: {part_one()}")  # 32835
    print(f"Part two answer: {part_two()}")  # 514662805187

    # Timers
    aoc.timer("part_one()", 1000, 5, globals())  # 1000 loops, best of 5: 3.4 ms
    aoc.timer("part_two()", 100, 5, globals())  # 100 loops, best of 5: 19 ms
