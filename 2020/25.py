"""--- Day 25: Combo Breaker ---"""

import adventofcode as aoc


def read_input(test_tag=""):
    input_path = aoc.input_path(2020, 25, test_tag)
    data = []
    with input_path.open("r") as input_file:
        for line in input_file:
            data.append(int(line))
    return data


def part_one(test=""):
    data = read_input(test)
    modulo = 20201227
    memory = [0 for _ in range(modulo)]
    value = 1
    n = 0
    while True:
        memory[value] = n
        value = value * 7 % modulo
        n += 1
        if memory[data[0]] and memory[data[1]]:
            break

    values = [
        (memory[data[0]], data[1]),
        (memory[data[1]], data[0]),
    ]
    rounds, subject = min(values, key=lambda x: x[0])
    answer = 1
    for _ in range(rounds):
        answer = answer * subject % modulo
    return answer


if __name__ == "__main__":
    # Part One
    assert part_one("test01") == 14897079
    print(f"Part one answer: {part_one()}")  # 11288669

    # Timers
    aoc.timer("part_one()", 10, 1, globals())  # 10 loops, best of 1: 8.4 s
