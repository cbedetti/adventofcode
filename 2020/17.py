"""--- Day 17: Conway Cubes ---"""

import adventofcode as aoc

import numpy as np
from scipy import signal


def read_input(test_tag=""):
    input_path = aoc.input_path(2020, 17, test_tag)
    data = []
    with input_path.open("r") as input_file:
        for line in input_file:
            data.append(list(line.rstrip()))
    data = np.array(data) == "#"
    return data


def conway_cubes_one_step(data):
    data = np.pad(data, 1)

    kernel = np.ones([3] * len(data.shape), dtype=int)
    kernel[tuple(1 for _ in range(len(data.shape)))] = 0

    data_conv = signal.convolve(data, kernel, mode="same")
    data[np.logical_and(data, ~np.isin(data_conv, [2, 3]))] = False
    data[np.logical_and(~data, np.isin(data_conv, [3]))] = True
    return data


def part_one(test=""):
    data = read_input(test)
    data = data[None, ...]
    for _ in range(6):
        data = conway_cubes_one_step(data)
    return np.sum(data)


def part_two(test=""):
    data = read_input(test)
    data = data[None, None, ...]
    for _ in range(6):
        data = conway_cubes_one_step(data)
    return np.sum(data)


if __name__ == "__main__":
    # Tests
    assert part_one("test01") == 112
    assert part_two("test01") == 848

    # Answers
    print(f"Part one answer: {part_one()}")  # 317
    print(f"Part two answer: {part_two()}")  # 1692

    # Timers
    aoc.timer("part_one()", 100, 5, globals())  # 100 loops, best of 5: 5.4 ms per loop
    aoc.timer("part_two()", 100, 5, globals())  # 100 loops, best of 5: 44 ms per loop
