"""--- Day 12: Rain Risk ---"""

import adventofcode as aoc

import numpy as np


def read_input(test_tag=""):
    input_path = aoc.input_path(2020, 12, test_tag)
    data = []
    with input_path.open("r") as input_file:
        for line in input_file:
            data.append([line[0], int(line[1:])])
    return data


class Ship:

    vectors = {
        "N": np.array([-1, 0]),
        "S": np.array([1, 0]),
        "E": np.array([0, 1]),
        "W": np.array([0, -1]),
    }
    orientations = ("N", "E", "S", "W")
    rotation = np.array([[0, -1], [1, 0]])

    def __init__(self, instructions):
        self.instructions = instructions
        self.position = np.array([0, 0])
        self.waypoint = np.array([-1, 10])
        self._orientation = 1

    @property
    def orientation(self):
        return self.orientations[self._orientation]

    @property
    def manhattan_distance(self):
        return np.abs(self.position).sum()

    def follow_instructions(self):
        for instruction, amplitude in self.instructions:
            if instruction in self.vectors.keys():
                self.move(instruction, amplitude)
            elif instruction == "F":
                self.move(self.orientation, amplitude)
            elif instruction in ["L", "R"]:
                amplitude //= 90
                if instruction == "L":
                    amplitude *= -1
                self._orientation = (self._orientation + amplitude) % 4

    def follow_good_instructions(self):
        for instruction, amplitude in self.instructions:
            if instruction in self.vectors.keys():
                self.move_waypoint(instruction, amplitude)
            elif instruction == "F":
                self.position += amplitude * self.waypoint
            elif instruction in ["L", "R"]:
                amplitude //= 90
                if instruction == "R":
                    turn = 1
                else:
                    turn = -1
                for _ in range(amplitude):
                    self.waypoint = self.waypoint.dot(turn * self.rotation)

    def move_waypoint(self, direction, amplitude):
        self.waypoint += amplitude * self.vectors[direction]

    def move(self, direction, amplitude):
        self.position += amplitude * self.vectors[direction]


def part_one(test=""):
    data = read_input(test)
    ship = Ship(data)
    ship.follow_instructions()
    return ship.manhattan_distance


def part_two(test=""):
    data = read_input(test)
    ship = Ship(data)
    ship.follow_good_instructions()
    return ship.manhattan_distance


if __name__ == "__main__":
    # Tests
    assert part_one("test01") == 25
    assert part_two("test01") == 286

    # Answers
    print(f"Part one answer: {part_one()}")  # 319
    print(f"Part two answer: {part_two()}")  # 50157

    # Timers
    aoc.timer("part_one()", 100, 5, globals())  # 100 loops, best of 5: 2.4 ms per loop
    aoc.timer("part_two()", 100, 5, globals())  # 100 loops, best of 5: 3.0 ms per loop
