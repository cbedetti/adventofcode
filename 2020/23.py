"""--- Day 23: Crab Cups ---"""

import adventofcode as aoc


def read_input(test_tag="", mistranslation=None):
    input_path = aoc.input_path(2020, 23, test_tag)
    with input_path.open("r") as input_file:
        for line in input_file:
            line = [int(_) for _ in line.rstrip()]

        cups = [0 for _ in range(len(line) + 1)]
        current_cup = line[0]
        for cup1, cup2 in zip(line[:-1], line[1:]):
            cups[cup1] = cup2

        if mistranslation:
            cups[line[-1]] = 10
            cups += range(11, mistranslation + 2)
            cups[mistranslation] = current_cup

        else:
            cups[line[-1]] = current_cup

    return current_cup, cups


class CrabGame:

    def __init__(self, current_cup, cups):
        self.current_cup = current_cup
        self.cups = cups
        self.max = max(cups)

    def __str__(self):
        cup = self.cups[self.current_cup]
        results = [self.current_cup]
        while cup != self.current_cup:
            results.append(cup)
            cup = self.cups[cup]
        return str(results)

    def answer_one(self):
        cup = self.cups[1]
        results = ""
        while cup != 1:
            results += str(cup)
            cup = self.cups[cup]
        return results

    def answer_two(self):
        cup = self.cups[1]
        return cup * self.cups[cup]

    def pick_up(self):
        pick_up = [self.cups[self.current_cup], None, None]
        for i, _ in enumerate(range(2)):
            pick_up[i + 1] = self.cups[pick_up[i]]
        return pick_up

    def destination(self, pick_up):
        cup = (self.current_cup - 1) % self.max or self.max
        while cup in pick_up:
            cup = (cup - 1) % self.max or self.max
        return cup, self.cups[cup]

    def move(self):
        pick_up = self.pick_up()
        after_pick_up = self.cups[pick_up[-1]]
        destination = self.destination(pick_up)

        self.cups[self.current_cup] = after_pick_up
        self.cups[destination[0]] = pick_up[0]
        self.cups[pick_up[-1]] = destination[1]

        self.current_cup = after_pick_up

    def play(self, turns):
        for _ in range(turns):
            self.move()


def part_one(test=""):
    crab_game = CrabGame(*read_input(test))
    crab_game.play(100)
    return crab_game.answer_one()


def part_two(test=""):
    crab_game = CrabGame(*read_input(test, 1000000))
    crab_game.play(10000000)
    return crab_game.answer_two()


if __name__ == "__main__":
    # Part One
    assert part_one("test01") == "67384529"
    print(f"Part one answer: {part_one()}")  # 25398647

    # Part Two
    assert part_two("test01") == 149245887792
    print(f"Part two answer: {part_two()}")  # 363807398885

    # Timers
    aoc.timer("part_one()", 100, 5, globals())  # 100 loops, best of 5: 0.34 ms
    aoc.timer("part_two()", 5, 1, globals())  # 5 loops, best of 1: 25 s
