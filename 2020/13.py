"""--- Day 13: Shuttle Search ---"""

import adventofcode as aoc

import numpy as np
from sympy import reduce_inequalities, symbols
from sympy.solvers.diophantine.diophantine import diop_solve


def read_input(test_tag=""):
    input_path = aoc.input_path(2020, 13, test_tag)
    with input_path.open("r") as input_file:
        lines = input_file.readlines()
        earliest = int(lines[0])
        bus_lines = lines[1].rstrip().split(",")
    return earliest, bus_lines


def part_one(test=""):
    earliest, bus_lines = read_input(test)
    bus_lines = [int(_) for _ in bus_lines if _ != "x"]
    waiting_times = [bus - (earliest % bus) for bus in bus_lines]
    return min(waiting_times) * bus_lines[np.argmin(waiting_times)]


def reduce_bus(bus1, bus2):
    """
    0  5  10  15  20  25  30  35  40  45  50  55  60  65  70  75  80  85  90  95
    0   7    14    21   28    35   42    49    56   63    70    77   84    91   98
    0            19              38             57             76             95
    https://en.wikipedia.org/wiki/Diophantine_equation
    ax + by = c
    """
    line1, shift1 = bus1
    line2, shift2 = bus2

    new_line = np.lcm.reduce([line1, line2])

    # a * line1 - shift1 = b * line2 - shift2
    a, b, t_0 = symbols("a, b, t_0", integer=True)
    resolution = diop_solve(a * line1 - shift1 - b * line2 + shift2)
    bounds = reduce_inequalities([resolution[0] > 0, resolution[1] > 0])
    parameter = np.ceil(bounds.lhs.evalf())
    new_shift = (line1 - resolution[1].subs({t_0: parameter})) * line2 + shift2

    return new_line, new_shift


def part_two(test=""):
    _, bus_lines = read_input(test)
    contest = []
    for shift, bus in enumerate(bus_lines):
        if bus != "x":
            contest.append((int(bus), shift))

    new_bus = reduce_bus(contest[0], contest[1])
    for bus in contest[2:]:
        new_bus = reduce_bus(new_bus, bus)
    line, shift = new_bus

    return line - shift


if __name__ == "__main__":
    # Tests
    assert part_one("test01") == 295
    assert part_two("test01") == 1068781

    # Answers
    print(f"Part one answer: {part_one()}")  # 2238
    print(f"Part two answer: {part_two()}")  # 560214575859998

    # Timers
    aoc.timer("part_one()", 100, 5, globals())  # 100 loops, best of 5: 0.13 ms per loop
    aoc.timer("part_two()", 100, 5, globals())  # 100 loops, best of 5: 91 ms per loop
