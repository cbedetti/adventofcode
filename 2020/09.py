"""--- Day 9: Encoding Error ---"""

import adventofcode as aoc

from collections import deque
from itertools import count, combinations


def read_input(test_tag=""):
    input_path = aoc.input_path(2020, 9, test_tag)
    data = []
    with input_path.open("r") as input_file:
        for line in input_file:
            data.append(int(line.rstrip()))
    return data


def part_one(test="", preamble_size=25):
    data = read_input(test)
    for i, number in enumerate(data[preamble_size:]):
        sums = []
        for num1, num2 in combinations(data[i : i + preamble_size], 2):
            sums.append(num1 + num2)
        if number not in sums:
            break
    return number


def part_two(test="", invalid_number=1212510616):
    def windows(data):
        for window_size in count(2):
            window = deque(data[:window_size], maxlen=window_size)
            yield list(window)
            for number in data[window_size:]:
                window.append(number)
                yield list(window)

    data = read_input(test)
    for window in windows(data):
        if sum(window) == invalid_number:
            break
    return min(window) + max(window)


if __name__ == "__main__":
    # Tests
    assert part_one("test01", 5) == 127
    assert part_two("test01", 127) == 62

    # Answers
    print(f"Part one answer: {part_one()}")  # 1212510616
    print(f"Part two answer: {part_two()}")  # 171265123

    # Timers
    aoc.timer("part_one()", 100, 5, globals())  # 100 loops, best of 5: 24 ms per loop
    aoc.timer("part_two()", 100, 5, globals())  # 100 loops, best of 5: 10 ms per loop
