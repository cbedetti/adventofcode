"""--- Day 15: Rambunctious Recitation ---"""

import adventofcode as aoc


def read_input(test_tag=""):
    input_path = aoc.input_path(2020, 15, test_tag)
    data = []
    with input_path.open("r") as input_file:
        for line in input_file:
            data.append([int(_) for _ in line.split(",")])
    return data[0]


def memory_game(data, length):
    # Init
    memory = [0] * length  # 25-30% plus rapide que defaultdict(lambda : 0)
    for step, number in enumerate(data[:-1], start=1):
        memory[number] = step

    next_number = data[-1]
    for step in range(len(data), length):
        last_when = memory[next_number]
        memory[next_number] = step
        if last_when == 0:
            next_number = 0
        else:
            next_number = step - last_when

    return next_number


def part_one(data=None):
    if not data:
        data = read_input()
    return memory_game(data, 2020)


def part_two(data=None):
    if not data:
        data = read_input()
    return memory_game(data, 30000000)


if __name__ == "__main__":
    # Tests
    assert part_one([0, 3, 6]) == 436
    assert part_one([1, 3, 2]) == 1
    assert part_one([2, 1, 3]) == 10
    assert part_one([1, 2, 3]) == 27
    assert part_one([2, 3, 1]) == 78
    assert part_one([3, 2, 1]) == 438
    assert part_one([3, 1, 2]) == 1836

    # Answers
    print(f"Part one answer: {part_one()}")  # 260
    print(f"Part two answer: {part_two()}")  # 950

    # Timers
    aoc.timer("part_one()", 100, 5, globals())  # 100 loops, best of 5: 0.38 ms per loop
    aoc.timer("part_two()", 10, 1, globals())  # 10 loops, best of 1: 8.8 s per loop
