"""--- Day 22: Crab Combat ---"""

import adventofcode as aoc

from collections import deque


def read_input(test_tag=""):
    input_path = aoc.input_path(2020, 22, test_tag)
    deck1 = deque()
    deck2 = deque()
    with input_path.open("r") as input_file:
        for line in input_file:
            if "Player 1" in line:
                deck = deck1
            elif "Player 2" in line:
                deck = deck2
            elif line == "\n":
                pass
            else:
                deck.append(int(line))
    return deck1, deck2


def combat(deck1, deck2):
    while True:
        card1, card2 = deck1.popleft(), deck2.popleft()

        if card1 > card2:
            deck1.extend([card1, card2])
        else:
            deck2.extend([card2, card1])

        if len(deck1) == 0:
            return deck2, "two"
        elif len(deck2) == 0:
            return deck1, "one"


def count_points(deck):
    result = 0
    for point, card in zip(range(len(deck), 0, -1), deck):
        result += point * card
    return result


def part_one(test=""):
    deck1, deck2 = read_input(test)
    winner_deck, _ = combat(deck1, deck2)
    return count_points(winner_deck)


def recursive_combat(deck1, deck2):
    past_rounds = set()
    while True:
        # Check game repetition
        state = (tuple(deck1), tuple(deck2))
        if state in past_rounds:
            return deck1, "one"
        else:
            past_rounds.add(state)

        # Playing a round
        card1, card2 = deck1.popleft(), deck2.popleft()

        # Who win the round
        if len(deck1) >= card1 and len(deck2) >= card2:
            _, round_winner = recursive_combat(
                deque(list(deck1)[:card1]),
                deque(list(deck2)[:card2]),
            )

        else:
            if card1 > card2:
                round_winner = "one"
            else:
                round_winner = "two"

        # Pick cards
        if round_winner == "one":
            deck1.extend([card1, card2])
        elif round_winner == "two":
            deck2.extend([card2, card1])

        # Check winner
        if len(deck1) == 0:
            return deck2, "two"
        elif len(deck2) == 0:
            return deck1, "one"


def part_two(test=""):
    deck1, deck2 = read_input(test)
    winner_deck, _ = recursive_combat(deck1, deck2)
    return count_points(winner_deck)


if __name__ == "__main__":
    # Part One
    assert part_one("test01") == 306
    print(f"Part one answer: {part_one()}")  # 32677

    # Part Two
    assert part_two("test02") == 105
    assert part_two("test01") == 291
    print(f"Part two answer: {part_two()}")  # 33661

    # Timers
    aoc.timer("part_one()", 100, 5, globals())  # 100 loops, best of 5: 0.55 ms
    aoc.timer("part_two()", 100, 5, globals())  # 100 loops, best of 5: 2.2 s
