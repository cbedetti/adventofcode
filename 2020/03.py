"""--- Day 3: Toboggan Trajectory ---"""

from adventofcode.puzzle import Puzzle


def check_trees(data, right, down):
    trees_len = len(data[0])
    check = 0
    trees_encounter = 0
    for trees in data[::down]:
        if trees[check] == "#":
            trees_encounter += 1
        check = (check + right) % trees_len
    return trees_encounter


class Soluce(Puzzle):
    test_solutions = [
        [7],  # Part One
        [336],  # Part Two
    ]

    def part_one(self):
        return check_trees(self.input, 3, 1)

    def part_two(self):
        slopes = [
            (1, 1),
            (3, 1),
            (5, 1),
            (7, 1),
            (1, 2),
        ]
        trees_encounter = 1
        for right, down in slopes:
            trees_encounter *= check_trees(self.input, right, down)
        return trees_encounter


if __name__ == "__main__":
    Soluce().solve()
