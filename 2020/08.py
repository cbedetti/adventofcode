"""--- Day 8: Handheld Halting ---"""

from adventofcode.puzzle import Puzzle


class GameCode:

    def __init__(self, code, flip_pointer=None):
        self.code = code
        self.code_len = len(code)
        self.accumulator = 0
        self.pointer = 0
        self.pointers = set()
        self.instructions = {
            "acc": self.add_accumulator,
            "jmp": self.jumps,
            "nop": self.no_operation,
        }
        self.flip_pointer = flip_pointer
        self.running = True
        self.ended = False
        while self.running:
            self.run_operation()

    def add_accumulator(self):
        self.accumulator += self.get_amount()
        self.pointer += 1

    def jumps(self):
        self.pointer += self.get_amount()

    def no_operation(self):
        self.pointer += 1

    def get_amount(self):
        arg = self.code[self.pointer][1]
        amount = int(arg[1:])
        if arg[0] == "-":
            amount *= -1
        return amount

    def run_operation(self):
        # Check if infinite loop
        if self.pointer in self.pointers:
            self.running = False
            return
        else:
            self.pointers.add(self.pointer)

        # Execute instruction
        instruction = self.code[self.pointer][0]
        if self.pointer == self.flip_pointer:
            if instruction == "jmp":
                instruction = "nop"
            elif instruction == "nop":
                instruction = "jmp"
        self.instructions[instruction]()

        # Check end of game
        if self.pointer >= self.code_len:
            self.ended = True
            self.running = False


class Soluce(Puzzle):
    test_solutions = [
        [5],  # Part One
        [8],  # Part Two
    ]

    def parser(self):
        self.data = []
        for line in self.input:
            self.data.append(line.split())

    def part_one(self):
        game = GameCode(self.data)
        return game.accumulator

    def part_two(self):
        for flip_pointer, code_line in enumerate(self.data):
            if code_line[0] in ["jmp", "nop"]:
                game = GameCode(self.data, flip_pointer)
                if game.ended:
                    break
        return game.accumulator


if __name__ == "__main__":
    Soluce().solve()
