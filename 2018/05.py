"""--- Day 5: Alchemical Reduction ---"""

from adventofcode.puzzle import Puzzle

from string import ascii_uppercase


def react(data):
    result = ["."]
    for unit in data:
        previous = result[-1]
        if unit != previous and unit.lower() == previous.lower():
            result.pop()
        else:
            result.append(unit)
    return len(result) - 1


class Soluce(Puzzle):
    test_solutions = [
        [10],  # Part One
        [4],  # Part Two
    ]

    def part_one(self):
        return react(self.input)

    def part_two(self):
        minimum_sizes = []
        for unit in ascii_uppercase:
            short_data = self.input.replace(unit, "").replace(unit.lower(), "")
            minimum_sizes.append(react(short_data))
        return min(minimum_sizes)


if __name__ == "__main__":
    Soluce().solve()
