"""--- Day 3: No Matter How You Slice It ---"""

from adventofcode.puzzle import Puzzle

from collections import Counter
from itertools import product


def compute_fabric(data):
    fabric = Counter()
    for x_start, y_start, x_size, y_size in data:
        fabric.update(
            product(
                range(x_start, x_start + x_size),
                range(y_start, y_start + y_size),
            )
        )
    return fabric


class Soluce(Puzzle):
    test_solutions = [
        [4],  # Part One
        [3],  # Part Two
    ]

    def parser(self):
        self.data = []
        for line in self.input:
            line = line.replace(":", "").split(" ")
            data_line = [int(_) for _ in line[2].split(",")]
            data_line += [int(_) for _ in line[3].split("x")]
            self.data.append(data_line)
        self.data

    def part_one(self):
        fabric = compute_fabric(self.data)
        return sum(_ > 1 for _ in fabric.values())

    def part_two(self):
        fabric = compute_fabric(self.data)

        for n, info in enumerate(self.data):
            claims = []
            for coord in product(
                range(info[0], info[0] + info[2]),
                range(info[1], info[1] + info[3]),
            ):
                claims.append(fabric[coord] == 1)
            if all(claims):
                return n + 1


if __name__ == "__main__":
    Soluce().solve()
