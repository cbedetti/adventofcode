"""--- Day 4: Repose Record ---"""

from adventofcode.puzzle import Puzzle

from collections import Counter, defaultdict


def compute_journal(data):
    journal = defaultdict(lambda: Counter())
    guard = None
    fall_time = None
    wake_time = None
    for event in data:
        event_split = event.split(" ")
        if event_split[2] == "Guard":
            guard = event_split[3]

        elif event_split[2] == "falls":
            fall_time = int(event_split[1][-3:-1])

        elif event_split[2] == "wakes":
            wake_time = int(event_split[1][-3:-1])
            for sleeping in range(fall_time, wake_time):
                journal[guard][sleeping] += 1

    return journal


class Soluce(Puzzle):
    test_solutions = [
        [240],  # Part One
        [4455],  # Part Two
    ]

    def part_one(self):
        self.input.sort()
        journal = compute_journal(self.input)

        selected_guard = None
        total_sleep = 0
        for guard, pattern in journal.items():
            sleep = sum(pattern.values())
            if sleep > total_sleep:
                total_sleep = sleep
                selected_guard = guard

        return journal[selected_guard].most_common(1)[0][0] * int(selected_guard[1:])

    def part_two(self):
        self.input.sort()
        journal = compute_journal(self.input)

        selected_guard = None
        selected_minute = None
        total_sleep = 0
        for guard, pattern in journal.items():
            for minute, sleep in pattern.items():
                if sleep > total_sleep:
                    total_sleep = sleep
                    selected_guard = guard
                    selected_minute = minute

        return selected_minute * int(selected_guard[1:])


if __name__ == "__main__":
    Soluce().solve()
