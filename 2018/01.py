"""--- Day 1: Chronal Calibration ---"""

from adventofcode.puzzle import Puzzle

from itertools import cycle


class Soluce(Puzzle):

    def parser(self):
        self.data = []
        for line in self.input:
            self.data.append([line[0], int(line[1:])])

    def part_one(self):
        frequency = 0
        for operation, amplitude in self.data:
            if operation == "+":
                frequency += amplitude
            else:
                frequency -= amplitude
        return frequency

    def part_two(self):
        frequencies = set([0])
        frequency = 0
        for operation, amplitude in cycle(self.data):
            if operation == "+":
                frequency += amplitude
            else:
                frequency -= amplitude

            if frequency in frequencies:
                return frequency
            else:
                frequencies.add(frequency)


if __name__ == "__main__":
    Soluce().solve()
