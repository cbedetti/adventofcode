"""--- Day 2: Inventory Management System ---"""

from adventofcode.puzzle import Puzzle

from collections import Counter
from itertools import combinations


class Soluce(Puzzle):
    test_solutions = [
        [12],  # Part One
        [None, "fgij"],  # Part Two
    ]

    def part_one(self):
        twos = 0
        threes = 0
        for line in self.input:
            counter = Counter(line)
            twos += 2 in counter.values()
            threes += 3 in counter.values()
        return twos * threes

    def part_two(self):
        for id1, id2 in combinations(self.input, 2):
            diff = 0
            diff_index = None
            for index, chars in enumerate(zip(id1, id2)):
                if chars[0] != chars[1]:
                    diff += 1
                    diff_index = index
                if diff == 2:
                    break
            if diff == 1:
                return id1[:diff_index] + id1[diff_index + 1 :]


if __name__ == "__main__":
    Soluce().solve()
