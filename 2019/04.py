"""--- Day 4: Secure Container ---"""

from adventofcode.puzzle import Puzzle

import re


def is_password_part1(number: int):
    """Is it a possible password"""
    is_adjacent = False
    is_increase = True
    number_string = str(number)

    for num0, num1 in zip(number_string[0:-1], number_string[1:]):
        if num0 == num1:
            is_adjacent = True
        if int(num0) > int(num1):
            is_increase = False

    return all([is_adjacent, is_increase])


def is_password_part2(number: int):
    """Is it a possible password"""
    is_adjacent = False
    is_simple_adjacent = False
    is_increase = True
    number_string = str(number)

    substrings = set()
    for num0, num1 in zip(number_string[0:-1], number_string[1:]):
        if num0 == num1:
            is_adjacent = True
            substrings.add(num0 * 2)
        if int(num0) > int(num1):
            is_increase = False

    if is_adjacent:
        occurrences = []
        for substring in substrings:
            occurrences.append(len(re.findall(f"(?={substring})", number_string)))
        is_simple_adjacent = 1 in occurrences

    return all([is_simple_adjacent, is_increase])


def possible_passwords(begin: int, last: int, is_password):
    """Yield possible passwords in a range"""
    good = 0
    for candidate in range(begin, last + 1):
        good += is_password(candidate)
    return good


class Soluce(Puzzle):

    def parser(self):
        self.data = [int(_) for _ in self.input.split("-")]

    def part_one(self):
        return possible_passwords(self.data[0], self.data[1], is_password_part1)

    def part_two(self):
        return possible_passwords(self.data[0], self.data[1], is_password_part2)


if __name__ == "__main__":
    Soluce().solve()
