"""--- Day 3: Crossed Wires ---"""

from adventofcode.puzzle import Puzzle

import numpy as np


class WirePath:
    def __init__(self, instructions):
        """Init"""
        self.instructions = instructions
        self.position = np.array([0, 0])
        self.signal_delay = 0
        self.path_data = {}
        self.compute()

    @property
    def path(self):
        """Return a set of the path"""
        return set(self.path_data.keys())

    def signal(self, position):
        """Get signal at a position"""
        return self.path_data.get(position)

    def compute(self):
        """Compute the path of the wire"""
        directions_dict = {"U": [1, 0], "D": [-1, 0], "L": [0, -1], "R": [0, 1]}
        for instruction in self.instructions:
            direction = instruction[0]
            move_len = int(instruction[1:])

            for _ in range(move_len):
                self.position += directions_dict[direction]
                self.add_to_path(self.position)

    def add_to_path(self, position):
        """Add to the path_data"""
        self.signal_delay += 1
        position_key = tuple(position)
        if position_key not in self.path_data.keys():
            self.path_data[position_key] = self.signal_delay


class Soluce(Puzzle):
    test_solutions = [
        [159, 135],  # Part One
        [610, 410],  # Part Two
    ]

    def parser(self):
        self.instructions = []
        for wire in self.input:
            self.instructions.append(wire.split(","))

    def part_one(self):
        """Find closest intersection from origin"""
        wires = [WirePath(_) for _ in self.instructions]
        intersections = wires[0].path.intersection(wires[1].path)
        distances = [np.abs(_).sum() for _ in intersections]
        return np.min(distances)

    def part_two(self):
        """Find the intersection with less delay"""
        wires = [WirePath(_) for _ in self.instructions]
        intersections = wires[0].path.intersection(wires[1].path)
        signals = [sum([wires[0].signal(_), wires[1].signal(_)]) for _ in intersections]
        return np.min(signals)


if __name__ == "__main__":
    Soluce().solve()
