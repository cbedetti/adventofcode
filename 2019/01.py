"""--- Day 1: The Tyranny of the Rocket Equation ---"""

from adventofcode.puzzle import Puzzle

import numpy as np


def fuel_counter(mass):
    """Compute fuel needed"""
    return int(np.floor(mass / 3) - 2)


def fuel_inception(mass):
    """Turns out we need more fuel"""
    total_fuel = 0
    while fuel_counter(mass) > 0:
        mass = fuel_counter(mass)
        total_fuel += mass
    return total_fuel


class Soluce(Puzzle):

    def parser(self):
        self.masses = map(int, self.input)

    def part_one(self):
        return sum(map(fuel_counter, self.masses))

    def part_two(self):
        return sum(map(fuel_inception, self.masses))


if __name__ == "__main__":
    Soluce().solve()
