""""""

import click
import json
import timeit
import numpy as np
import importlib.util
import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from itertools import product

from adventofcode.starter import TODAY, YEAR, DAY
from adventofcode.treepath import TreePath


def import_soluce_from_path(treepath):
    # Create a module spec from the given path
    spec = importlib.util.spec_from_file_location("soluce", treepath.soluce)

    # Load the module from the created spec
    module = importlib.util.module_from_spec(spec)

    try:
        spec.loader.exec_module(module)
        return module.Soluce(year=treepath.year, day=treepath.day)
    except FileNotFoundError as error:
        raise SystemExit(error)


def update_timings_records(treepath, part_name, average_ms):
    year = treepath.year
    day = treepath.day

    try:
        with treepath.timings.open("r") as data:
            timings = json.load(data)
    except FileNotFoundError:
        timings = {}

    key = f"{year}-{part_name}"
    if not timings.get(key):
        timings[key] = [None] * 26

    record = timings[key][day]
    if record is None or average_ms < record:
        timings[key][day] = average_ms
        with treepath.timings.open("w") as output:
            json.dump(timings, output, indent=4, sort_keys=True)


def make_heatmap():
    treepath = TreePath()
    with treepath.timings.open("r") as data:
        timings = json.load(data)

    timings = pd.DataFrame.from_dict(timings, dtype=float).drop(0)
    timings.index.name = "Day"

    for year, part in product(range(2015, treepath.year + 1), ["part01", "part02"]):
        name = f"{year}-{part}"
        if not name in timings.columns:
            timings[name] = np.nan

    timings = timings.reindex(sorted(timings.columns), axis=1)

    plt.style.use("dark_background")
    heatmap = sns.heatmap(
        timings,
        cmap="plasma",
        linewidth=0.5,
        linecolor="black",
        norm=LogNorm(0.1, 20000),
        cbar_kws={"format": "%.1g"},
    )

    heatmap.tick_params("x", length=0)
    heatmap.tick_params("y", length=0)

    years_label = [year for year in range(2015, treepath.year + 1)]
    heatmap.set_xticks(
        list(map(lambda x: x + 0.5, heatmap.get_xticks()[0::2])),
        labels=years_label,
        rotation=None,
    )

    heatmap.vlines(
        list(map(lambda x: x + 1, heatmap.get_xticks())),
        0,
        25,
        linestyle="dashed",
        linewidth=0.5,
        color="white",
    )

    heatmap.set_title("Timings of Advent of Code solutions (ms)")
    plt.text(
        x=-1,
        y=28,
        s=f"last update: {TODAY}",
        fontsize="x-small",
        fontstyle="italic",
        color="white",
    )
    plt.savefig(treepath.repo_dir / "timings.png")


@click.command()
@click.option("--repeat", "-r", type=int, default=15, help="Number of try to do")
@click.option(
    "--best", "-b", type=int, default=3, help="Number of tries to average over"
)
@click.option("--one", is_flag=True, help="Timing only part one")
@click.argument("year", type=int, default=YEAR, nargs=1)
@click.argument("day", type=int, default=DAY, nargs=1)
def timer(year, day, repeat, best, one):
    """Timing a DAY (example: 10 INTEGER) of advent of code of the YEAR (example: 2024 INTEGER) you want"""
    treepath = TreePath(year, day)
    soluce = import_soluce_from_path(treepath)
    environement = globals()
    environement.update({"soluce": soluce})

    for part in range(2):
        if soluce.solve_part(part):
            part_name = f"part{part+1:02d}"
            results = timeit.repeat(
                f"soluce.solve_part({part})",
                repeat=repeat,
                number=1,
                globals=environement,
            )
            results.sort()
            average_ms = np.mean(results[:best]) * 1000
            print(
                f"{year}-{day:02d} {part_name}, {repeat} loops, best of {best}: {average_ms:.3g} ms per loop"
            )

            update_timings_records(treepath, part_name, average_ms)

            if one:
                break

    make_heatmap()
