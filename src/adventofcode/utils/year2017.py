from itertools import islice, cycle


class KnotHash:
    MEM_SIZE = 256
    SALT = [17, 31, 73, 47, 23]
    HASHING_REPEAT = 64

    def __init__(self, mem_size=MEM_SIZE):
        self.mem_size = mem_size
        self.memory = list(range(mem_size))
        self.knot_hash = ""

    def hashing(self, key, repeat=HASHING_REPEAT):
        if type(key) == str:
            sequence = [ord(_) for _ in key] + self.SALT
        else:
            # for part one of 2017day10
            sequence = key

        offset = 0
        for skip_size, length in enumerate(
            islice(cycle(sequence), len(sequence) * repeat)
        ):
            skip = skip_size % self.mem_size
            self.memory = self.memory[length:] + self.memory[:length][::-1]
            self.memory = self.memory[skip:] + self.memory[:skip]
            offset -= length + skip

        offset %= self.mem_size
        self.memory = self.memory[offset:] + self.memory[:offset]

        if self.mem_size == self.MEM_SIZE:
            self.hex_convert()

    def hex_convert(self):
        dense_hash = []
        sparse_hash = [iter(self.memory)] * 16
        for block in zip(*sparse_hash):
            block = iter(block)
            item = next(block)
            for next_item in block:
                item ^= next_item
            dense_hash.append(item)
        self.knot_hash = "".join(map(lambda _: "{:02x}".format(_), dense_hash))


from collections import defaultdict


class Cpu:
    def __init__(self):
        self.registers = defaultdict(lambda: 0)
        self.pointer = 0

    def run(self, instructions):
        self.running = True
        while self.running:
            self.execute(instructions[self.pointer])

    def execute(self, instruction):
        command = instruction[0]
        args = instruction[1:]
        if len(args) == 2 and isinstance(args[1], str):
            args[1] = self.registers[args[1]]
        getattr(self, command)(*args)

    def snd(self):
        return NotImplemented

    def set(self, register, value):
        self.registers[register] = value
        self.pointer += 1

    def add(self, register, value):
        self.registers[register] += value
        self.pointer += 1

    def mul(self, register, value):
        self.registers[register] *= value
        self.pointer += 1

    def mod(self, register, value):
        self.registers[register] %= value
        self.pointer += 1

    def rcv(self):
        return NotImplemented

    def jgz(self, register, value):
        if isinstance(register, str):
            compare = self.registers[register]
        else:
            compare = int(register)

        if compare > 0:
            self.pointer += value
        else:
            self.pointer += 1
