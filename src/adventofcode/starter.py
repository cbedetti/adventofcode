"""Advent of Code, helpers functions"""

import click
import logging
from datetime import date
from jinja2 import Template
from bs4 import BeautifulSoup
from configparser import ConfigParser
from urllib.request import Request, urlopen

from adventofcode.treepath import TreePath


TODAY = date.today()
YEAR = TODAY.year
DAY = TODAY.day
URL_AOC = "https://adventofcode.com"
SECRET = ConfigParser()
SECRET.read(TreePath.repo_dir / "secret.txt")


def download_webpage(url):
    request = Request(
        url,
        headers={
            "User-Agent": SECRET.get("aoc", "user_agent"),
            "cookie": f'session={SECRET.get("aoc", "session")}',
        },
    )
    return urlopen(request).read().decode("utf-8")


def download_day_description(year=YEAR, day=DAY):
    url = f"{URL_AOC}/{year}/day/{day}"
    html_text = download_webpage(url)
    soup = BeautifulSoup(html_text, features="html.parser")
    elements = soup.find("h2")
    try:
        day_description = elements.contents[0]
        if f"Day {day}" in day_description:
            return day_description
    except Exception as error:
        logging.exception(error)


def download_input(year=YEAR, day=DAY):
    input_path = TreePath(year, day).input()
    if not input_path.exists():
        url = f"{URL_AOC}/{year}/day/{day}/input"
        input_text = download_webpage(url)
        input_path.write_text(input_text)


@click.command()
@click.option(
    "--test", "-t", type=int, default=1, help="Number of test file(s) to create"
)
@click.argument("year", type=int, default=YEAR, nargs=1)
@click.argument("day", type=int, default=DAY, nargs=1)
def starter(year, day, test):
    """Prepare a DAY (example: 10 INTEGER) of advent of code of the YEAR (example: 2024 INTEGER) you want"""
    tree_path = TreePath(year, day)
    tree_path.year_dir.mkdir(exist_ok=True)
    tree_path.tests_dir.mkdir(exist_ok=True)
    download_input(year, day)

    if not tree_path.soluce.exists():
        template = Template(tree_path.template.open("r").read())
        tags = {
            "year": year,
            "day": day,
            "day_description": download_day_description(year, day),
        }
        template.stream(**tags).dump(str(tree_path.soluce))

    for count in range(test):
        tree_path.input_test(count + 1).touch()
