""""""

from datetime import date
from pathlib import Path


TODAY = date.today()
YEAR = TODAY.year
DAY = TODAY.day


class TreePath:

    module_dir = Path(__file__).parent
    repo_dir = module_dir.parent.parent

    def __init__(self, year=YEAR, day=DAY):
        self.year = year
        self.day = day

        # Directories
        self.year_dir = self.repo_dir / str(year)
        self.tests_dir = self.year_dir / "tests"
        self.data_dir = self.repo_dir / "data"

        # Files
        self.template = self.module_dir / "templates" / "starter.py"
        self.timings = self.data_dir / "timings.json"
        self.soluces = self.data_dir / "soluces.json"
        self.soluce = self.year_dir / f"{self.day:02d}.py"

    def input(self, tag=""):
        filename = f"{self.year}-{self.day:02d}"
        if tag:
            filename += f"_{tag}"
        return self.repo_dir / "inputs" / f"{filename}.txt"

    def input_test(self, count):
        return self.tests_dir / f"{self.day:02d}_test{count:02d}.txt"

    def test_inputs(self):
        for path in self.tests_dir.glob(f"{self.day:02d}_test*.txt"):
            yield path
