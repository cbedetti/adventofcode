"""Advent of Code puzzle solver."""

import inspect
import matplotlib.pyplot as plt
import time
import json
from pathlib import Path

from adventofcode.treepath import TreePath


class Inputs:

    def __init__(self, year, day, tag=""):
        treepath = TreePath(year, day)
        self.input = self.read_input(treepath.input(tag))
        self.test_inputs = [self.read_input(path) for path in treepath.test_inputs()]

    @staticmethod
    def read_input(path):
        with path.open("r") as input_file:
            input = [line.rstrip("\n\r") for line in input_file]

        if len(input) == 1:
            input = input[0]

        return input


class Puzzle:

    test_solutions = (
        [],  # Part One
        [],  # Part Two
    )

    def __init__(self, tag="", year=None, day=None):
        if year and day:
            pass
        else:
            self_file = Path(inspect.getmodule(self).__file__)
            year = int(self_file.parts[-2])
            day = int(self_file.stem)

        self.treepath = TreePath(year, day)
        self.parts = [self.part_one, self.part_two]

        inputs = Inputs(year, day, tag)
        self.master_input = inputs.input
        self.test_inputs = inputs.test_inputs
        self.testing = False

        with self.treepath.soluces.open("r") as data:
            self.soluces = json.load(data)[str(year)][f"{day:02d}"]

    def save_array(self, array, filename, cmap="viridis", clobber=False):
        output = self.treepath.year_dir / filename
        if clobber or not output.exists():
            plt.imshow(array, cmap=cmap)
            plt.savefig(output)

    def parser(self):
        pass

    def part_one(self):
        return None

    def part_two(self):
        return None

    def solve_part(self, part, test_input=None):
        self.input = self.master_input
        if test_input:
            self.input = test_input
        self.parser()
        return self.parts[part]()

    def solve_tests(self, part):
        self.testing = True
        count = 1
        for test_input, solution in zip(self.test_inputs, self.test_solutions[part]):
            if solution is not None:
                attempt = self.solve_part(part, test_input)

                if attempt != solution:
                    message = (
                        f"Test {count}: |attempt| {attempt} <{type(attempt).__name__}>"
                        f" != |solution| {solution} <{type(solution).__name__}>"
                    )
                    raise SystemExit(message)
            count += 1
        self.testing = False

    def solve_part_with_timing(self, part):
        start = time.process_time()
        solution = self.solve_part(part)
        duration = round(1000 * (time.process_time() - start), 3)

        part_name = self.parts[part].__name__.replace("_", " ").capitalize()
        message = f"{part_name} in {duration} ms: {solution}"

        if self.soluces[part] != solution:
            message += f" ({self.soluces[part]} in soluces.json)"

        print(message)

    def solve(self):
        for part in range(2):
            self.solve_tests(part)
            self.solve_part_with_timing(part)
