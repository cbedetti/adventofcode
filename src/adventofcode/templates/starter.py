"""{{day_description}}"""

from adventofcode.puzzle import Puzzle

import re  # noqa
import math  # noqa
import numpy as np  # noqa
import networkx as nx  # noqa
import matplotlib.pyplot as plt  # noqa
from tqdm import tqdm  # noqa
from string import ascii_letters, ascii_lowercase, ascii_uppercase  # noqa
from collections import Counter, defaultdict, deque, namedtuple  # noqa
from itertools import (
    count,
    product,
    permutations,
    combinations,
    combinations_with_replacement,
)  # noqa


# Itertools Functions:
# product('ABCD', repeat=2)            AA AB AC AD BA BB BC BD CA CB CC CD DA DB DC DD
# permutations('ABCD', 2)                          AB AC AD BA BC BD CA CB CD DA DB DC
# combinations('ABCD', 2)                                            AB AC AD BC BD CD
# combinations_with_replacement('ABCD', 2)               AA AB AC AD BB BC BD CC CD DD


class Soluce(Puzzle):
    test_solutions = [
        [],  # Part One
        [],  # Part Two
    ]

    def parser(self):
        print(self.input)

    def part_one(self):
        return super().part_one()

    def part_two(self):
        return super().part_two()


if __name__ == "__main__":
    Soluce().solve()
